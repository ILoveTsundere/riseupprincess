﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using XRPGLibrary;
using XRPGLibrary.Controls;

//using XRPGLibrary.WorldClasses;

namespace RiseUpPrincess.GameScreens {
    public class GameTitleScreen : BaseGameState {
        #region Constructor Region

        public GameTitleScreen(Game1 game, GameStateManager manager)
            : base(game, manager) {
        }

        #endregion

        #region Field Region

        private PictureBox backgroundImage;
        private LinkLabel startGame;
        private LinkLabel loadGame;
        private LinkLabel options;
        private LinkLabel exitGame;
        private float maxItemWidth;
        //private SoundEffect gameMusic;
        //private SoundEffect switchSelection;
        //private SoundEffect enterSelection;

        #endregion

        #region Property Region

        #endregion

        #region Method Region

        public override void Initialize() {
            base.Initialize();
        }

        protected override void LoadContent() {
            base.LoadContent();
            backgroundImage = new PictureBox(Game.Content.Load<Texture2D>(@"Backgrounds\titlescreen"), GameRef.ScreenRectangle);
            ControlManager.Add(backgroundImage);
            startGame = new LinkLabel {
                Text = "New Game",
                Color = Color.WhiteSmoke,
                Size = startGame.SpriteFont.MeasureString(startGame.Text)
            };

            startGame.Selected += menuItem_Selected;
            ControlManager.Add(startGame);

            loadGame = new LinkLabel {
                Text = "Continue",
                Color = Color.WhiteSmoke,
                Size = loadGame.SpriteFont.MeasureString(loadGame.Text)
            };
            loadGame.Selected += menuItem_Selected;
            ControlManager.Add(loadGame);

            options = new LinkLabel {
                Text = "Options",
                Color = Color.WhiteSmoke,
                Size = options.SpriteFont.MeasureString(options.Text)
            };
            options.Selected += menuItem_Selected;
            ControlManager.Add(options);

            exitGame = new LinkLabel {
                Text = "Exit",
                Color = Color.WhiteSmoke,
                Size = exitGame.SpriteFont.MeasureString(exitGame.Text)
            };
            exitGame.Selected += menuItem_Selected;
            ControlManager.Add(exitGame);
            ControlManager.NextControl();

            maxItemWidth = startGame.Size.X;
            var position = new Vector2(450, 500);
            foreach (Control c in ControlManager) {
                if (!(c is LinkLabel))
                    return;

                c.Position = new Vector2(position.X + ((maxItemWidth - c.Size.X)/2), position.Y);
                position.Y += c.Size.Y + 5f;
            }
        }

        public override void Update(GameTime gameTime) {
            ControlManager.Update(gameTime, PlayerIndexInControl);
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime) {
            GameRef.SpriteBatch.Begin();
            base.Draw(gameTime);
            ControlManager.Draw(GameRef.SpriteBatch);
            GameRef.SpriteBatch.End();
        }

        #endregion

        #region Event Handlers

        public void menuItem_Selected(object sender, EventArgs e) {
            if (sender == startGame) {
                InputHandler.Flush();
                //StateManager.PushState(GameRef.BattleScreen);
            }
            else if (sender == loadGame) {
                //StateManager.PushState(GameRef.WorldViewScreen);
            }
            else if (sender == options) {
            }
            else if (sender == exitGame) {
            }
        }


        private void CreateWorld() {
        }

        #endregion
    }
}