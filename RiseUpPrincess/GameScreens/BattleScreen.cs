﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using RiseUpPrincess.GameComponents;
using RiseUpPrincess.GameScreens.HUD;
using TiledSharp;
using XRPGLibrary;
using XRPGLibrary.CharacterAbilities;
using XRPGLibrary.CharacterClasses;
using XRPGLibrary.TileEngine;

//using XRPGLibrary.WorldClasses;

namespace RiseUpPrincess.GameScreens {
    public class BattleScreen : BaseGameState {
        #region Constructor Region

        public BattleScreen(Game1 game, GameStateManager manager)
            : base(game, manager) {
            _gameRef = game;
            _isChosen = false;
            _totalPathSet = new HashSet<MapCell>();
        }

        #endregion

        #region Field Region

        public enum InputState {
            Idle,
            Menu, //draws the menu class and shit
            HUD, //should check if mouse is within the HUD display so it handles input differently
            ActingCharacter,
            CharacterSelected,
            EnemyTurn,
            OtherTurn
        };

        private BattleScreenHUD _battleHUD;
        private InputState _currentInputState;
        private InputState _lastInputState;
        private SpriteFont _pericles6;
        private Texture2D _hilight;
        private readonly Game1 _gameRef;
        private TileMap _myMap;
        private Texture2D _tileSetTexture;
        private TmxMap _map;
        private HashSet<MapCell> _totalPathSet;
        private MapCell _lastTile; //last highlited tile
        private MapCell _chosenTile; //chosen tile, by the player
        private MapCell _lastChosenTile; // last chosenTile, I'll use this so it doesn't keep redoing the pathing and shit
        private bool _isChosen; //checks if a tile has been chosen, this could probably be changed to a null check in chosentile
        private Character _chosenCharacter; //checks if there's a moving character, this is used for disabling input and other things
        private VectorPath _pathing; //the vector pathing class
        private List<TileMap> _mapLayers;
        private Vector2 _lastHilightPos;
        //private List<MapCell> _enemyBattleCharacters; //list of enemies in the stage
        //private List<MapCell> _otherBattleCharacters; //list of enemies in the stage
        private Character _princess;
        private Character _princess2;
        private Vector2 _hilightPos;
        private string _action;

        #endregion

        #region Method Region

        public override void Initialize() {
            base.Initialize();
        }

        protected override void LoadContent() {
            base.LoadContent();
            _battleHUD = new BattleScreenHUD(ControlManager, GameRef, this);
            Character.GameRef = _gameRef;
            _mapLayers = new List<TileMap>();
            _princess = new Princess("MC");
            _princess.LoadContent();
            _princess2 = new Princess("MC");
            _princess2.LoadContent();
            _pericles6 = _gameRef.Content.Load<SpriteFont>(@"Fonts\Pericles6");
            _map = new TmxMap(@"Content\test.tmx");
            string version = _map.Version;
            TmxTileset myTileset = _map.Tilesets[0];
            TmxLayer myLayer = _map.Layers[0];
            _hilight = _gameRef.Content.Load<Texture2D>(@"Tilesets\hilight");
            _tileSetTexture = _gameRef.Content.Load<Texture2D>(@"TileSets\part4_tileset");
            _myMap = new TileMap(_map.Height, _map.Width);
            Camera2D.SpriteHeight = _map.TileHeight;
            Camera2D.SpriteWidth = _map.TileWidth;
            Camera2D.MapHeight = _map.Height;
            Camera2D.MapWidth = _map.Width;
            Camera2D.MoveCamera(new Vector2(0, 0));

            SetUpMap();

            _lastTile = _myMap.Tile(0, 0);
            _pathing = new VectorPath(_myMap);
            _myMap.Tile(0, 1).SetOccupant(_princess);
            _myMap.Tile(4, 5).SetOccupant(_princess2);
            _princess2.SpriteAnimation.Position = new Vector2((4*32 - 5*32 + _myMap.Tile(4, 5).Ocupee.SpriteAnimation.Width/2), (4*16 + 5*16) - 22 - _myMap.Tile(4, 5).Height);
            _princess.SpriteAnimation.Position = new Vector2((0*32 - 1*32 + _myMap.Tile(0, 1).Ocupee.SpriteAnimation.Width/2), (0*16 + 1*16) - 22 - _myMap.Tile(0, 1).Height);
            //_portraitBox = new PictureBox(null, Rectangle.Empty);
            //ControlManager.Add(_portraitBox);

            _currentInputState = InputState.Idle;
        }

        protected override void UnloadContent() {
            base.UnloadContent();
        }


        private void SetUpMap() {
            //TODO: store the layers
            var height = 0; //height goes up as tiles are added to the same place
            var tileNum = 0; //used to figure out what tile I'm on in the TMX format map
            foreach (TmxLayer layer in _map.Layers) {
                var map = new TileMap(_map.Height, _map.Width);
                height += 10;
                for (var y = 0; y < _map.Height; ++y) {
                    for (var x = 0; x < _map.Width; ++x) {
                        int tileId = layer.Tiles[tileNum++].Gid - 1;
                        _myMap.AddTile(x, y, tileId);
                        map.AddTile(x, y, tileId);
                        map.Rows[y].Columns[x].Height = 0;

                        if (_myMap.Rows[y].Columns[x].TileId == -1)
                            continue;
                        _myMap.Rows[y].Columns[x].Height = height;
                        map.Rows[y].Columns[x].Height = height;


                        /*if(_myMap.Rows[y].Columns[x].Ocuupe.Alliance == "Enemy"){
                         * enemyBattleCharacters.add(_myMap.Rows[y].Columns[x].Ocuupe); //add that enemy to the list of enemies
                         * } */
                        /*else if(_myMap.Rows[y].Columns[x].Ocuupe.Alliance == "Other") {
                         * otherBattlerCharacters.add(_myMap.Rows[y].Columns[x].Ocuupe); //add other to it, this is if there's ever a 3rd party
                         * }*/
                    }
                }
                _mapLayers.Add(map);
                tileNum = 0;
            }
        }

        /// <summary>
        ///     Updates the battlescreen, update characters has to come before any other checks for reasons... unknown.
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime) {
            switch (_currentInputState) {
                case InputState.Menu:

                    GrabGameInput(gameTime);
                    //UpdateMenu();
                    //return to do nothing, no updaaaates
                    return; //simple return so it doesn't update anything else

                case InputState.Idle:
                    UpdateHighlightPos();
                    UpdateCharacters(gameTime);
                    if (Camera2D.CheckBoundaryDistance(new Vector2(InputHandler.MouseState.Position.X, InputHandler.MouseState.Position.Y)))
                        Camera2D.UpdateMove(_lastHilightPos);

                    //TODO: Do normal map input and movement
                    GrabGameInput(gameTime);
                    //UpdateHUD(gameTime);
                    break;

                case InputState.CharacterSelected:
                    UpdateCharacters(gameTime);
                    UpdateHighlightPos();
                    _battleHUD.Update(gameTime);
                    //TODO: show the character info and a menu dependant on the characters state
                    GrabGameInput(gameTime);
                    //UpdateHUD(gameTime);
                    break;

                case InputState.ActingCharacter:
                    UpdateCharacters(gameTime);
                    UpdateHighlightPos();
                    //Camera2D.CenterOn(_chosenCharacter.SpriteAnimation.Position);
                    switch (_chosenCharacter.CurrentState) {
                        case (Character.CharacterStates.Moving):
                            if (_chosenCharacter.SpriteAnimation.IsMoving) break;
                            _chosenTile.Occupied = false;
                            //_chosenCharacter.Action = false;
                            _currentInputState = InputState.Idle; //change it back to characterselected once I add attack and shit
                            _chosenCharacter.CurrentState = Character.CharacterStates.Moved;
                            _chosenCharacter = null;
                            break;

                        case (Character.CharacterStates.Moved): //Not needed?
                            break;
                        case (Character.CharacterStates.Attacking):
                            //if (_chosenCharacter.SpriteAnimation.isAttacking) return;

                            break;
                        case (Character.CharacterStates.Done): //Not needed?
                            break;
                    }

                    break;

                case InputState.EnemyTurn:
                case InputState.OtherTurn:
                    UpdateCharacters(gameTime);

                    //only able to change to menu input and back
                    //both states will be handled the same way in regards to player input

                    break;
            }

            //TODO: Enemy and Ally updates and animations


            base.Update(gameTime);
        }


        private void UpdateCharacters(GameTime gameTime) {
            Player.Update(gameTime);
            //TODO: Update every character on screen
            _princess.Update(gameTime);
            _princess2.Update(gameTime);
        }

        private void UpdateHighlightPos() {
            Vector2 hilightPos = Camera2D.ScreenToWorld(new Vector2(InputHandler.MouseState.X - 32, InputHandler.MouseState.Y)); //TODO: CLAMP THE VALUES USING THE POWER OF MATH
            var tile = new Point(((int) (hilightPos.X/32 + hilightPos.Y/16)/2), (int) (hilightPos.Y/16 - hilightPos.X/32)/2);

            if (_myMap.TileExists(tile.X, tile.Y)) {
                //check if that specific tile exists, so I can highlight it...  //TODO: think of doing this in another method if possible, to clean it up

                _hilightPos = new Vector2((tile.X*32 - tile.Y*32), (tile.X*16 + tile.Y*16) - _myMap.Tile(tile.X, tile.Y).Height);
                _lastHilightPos = _hilightPos;
                _lastTile = _myMap.Tile(tile.X, tile.Y);
            }
            else {
                _hilightPos = _lastHilightPos;
            }
            ShowInfo();
        }

        private void ShowInfo() {
            if (_totalPathSet.Count <= 0) return;
            if (_totalPathSet.Contains(_lastTile) && _lastTile.Occupied)
                _battleHUD.ShowActionInfo(_lastTile.Ocupee, _action);
        }


        private void DrawHUD() {
            if (_currentInputState == InputState.Menu) {
                Vector2 ssize = _pericles6.MeasureString("PAUSED!");
                GameRef.SpriteBatch.DrawString(_pericles6, "PAUSED!", new Vector2((Camera2D.ViewportWidth - ssize.X)/2, (Camera2D.ViewportHeight - ssize.Y)/2), Color.Black);
            }
            ControlManager.Draw(GameRef.SpriteBatch);
        }

        public void GrabGameInput(GameTime gameTime) {
            //if its currently in menu, only handle to see if ESC is being pressed
            //TODO: Make an actual menu
            if (_currentInputState == InputState.Menu) {
                if (InputHandler.KeyPressed(Keys.Escape)) {
                    _currentInputState = _lastInputState;
                }
                //UpdateMenu();
                return;
            }

            if (InputHandler.KeyPressed(Keys.Escape)) {
                _lastInputState = _currentInputState;
                _currentInputState = InputState.Menu;
            }

            /*
            //TODO: bring back the abily to choose and shit
            if(InputHandler.KeyDown(Keys.W)) {
                Camera2D.MoveCamera(new Vector2(0, -10));
            }
            if(InputHandler.KeyDown(Keys.S)) {
                Camera2D.MoveCamera(new Vector2(0, 10));
            }
            if(InputHandler.KeyDown(Keys.D)) {
                Camera2D.MoveCamera(new Vector2(10, 0));
            }
            if(InputHandler.KeyDown(Keys.A)) {
                Camera2D.MoveCamera(new Vector2(-10, 0));
            }
            */
            if (InputHandler.RightMouseReleased()) {
                if (_currentInputState != InputState.Idle)
                    CharacterDeselect();
                else {
                    _battleHUD.ToggleTurnMenu();
                }
            }

            if (InputHandler.LeftMouseReleased()) {
                // if a tile(character) was chosen
                Console.WriteLine(_action);
                if (_action == null) {
                    if (_lastTile.Occupied) {

                        _chosenTile = _lastTile; // set it as the chosen tile
                        _chosenCharacter = _chosenTile.Ocupee;
                        _battleHUD.ChosenCharacter(_chosenCharacter);
                        _currentInputState = InputState.CharacterSelected;
                    } else {
                        CharacterDeselect();
                    }
                }
                else {
                    if (_totalPathSet.Contains(_lastTile))
                        DoAction();
                    else {
                        if (_lastTile.Occupied) {
                            if (_lastTile.Ocupee.CurrentState == Character.CharacterStates.Done)
                                return;
                            _chosenTile = _lastTile; // set it as the chosen tile
                            _chosenCharacter = _chosenTile.Ocupee;
                            _battleHUD.ChosenCharacter(_chosenCharacter);
                            _currentInputState = InputState.CharacterSelected;
                        }
                        else {

                            _currentInputState = InputState.Idle;
                            CharacterDeselect();
                        }
                    }
                }
            }
        }

        public void CheckPathForAction ( string action ) {
            _action = action;
            _totalPathSet.Clear();
            Console.WriteLine(_action);
            switch(action) {
                case "Move":
                    GetPossiblePaths();
                    break;
                case "Defend":
                    _totalPathSet = _pathing.GetNeighborTiles(_chosenTile);
                    break;
                case "Stay":
                    //ends the characters turn immediately, no action.
                    DoAction();
                    break;
                case "Attack":
                    _totalPathSet = _pathing.AttackRange(_chosenTile);
                    break;
            }
        }

        private void GetPossiblePaths () {
            _totalPathSet = _pathing.GetTotalPath(_chosenTile);
        }

        public void UseSkill ( string selectedSkill ) {
            //then end that character's turn
        }

        public void UseMagic ( string selectedSpell ) {
        }

        public void UseItem ( string selectedItem ) {
        }

        public void CheckMagicRange () {
            _action = "Magic";
        }

        public void CheckSkillRange ( Skill skill ) {
            //TODO: Check the skills range, etc
            _action = "Skill";
        }

        public void CheckItemRange () {
            _action = "Item";
        }

        /// <summary>
        ///     Clears the path set, made into a method so other battlescreen hud can call to clear it.
        /// </summary>
        public void ClearPathing () {
            _totalPathSet.Clear();
        }

        private void DoAction() {
            switch (_action) {
                case "Move":
                    if (!_lastTile.Occupied) {
                        var pathing = new Queue<MapCell>(_pathing.ShortestPathTo(_chosenTile, _lastTile).Reverse());
                        MapCell lastTile = pathing.Last();
                        _chosenCharacter = _chosenTile.Ocupee;
                        _chosenTile.Ocupee.SpriteAnimation.MoveQueue(pathing, _chosenTile);
                        lastTile.Ocupee = _chosenTile.Ocupee;
                        lastTile.Occupied = true;
                        _totalPathSet.Clear();
                        _chosenCharacter.CurrentState = Character.CharacterStates.Moving;
                        _currentInputState = InputState.ActingCharacter;
                    }

                    break;
                case "Magic":
                    UseMagic(_action);
                    break;

                case "Skill":
                    UseSkill(_action);
                    break;

                case "Item":
                    UseItem(_action);
                    break;

                case "Defend":
                    _chosenCharacter.SpriteAnimation.FaceDirectionOfCell(_chosenTile, _lastTile);
                    //TODO: Add a method to Character that gives it a 1 turn modifier for more defense/magic defense. Maybe also agility
                    _chosenCharacter.CurrentState = Character.CharacterStates.Done;
                    CharacterDeselect();
                    break;

                case "Attack":
                    if (_lastTile.Occupied) {
                        _chosenCharacter.CurrentState = Character.CharacterStates.Attacking;
                    }
                    break;

                case "Stay":
                    _chosenCharacter.CurrentState = Character.CharacterStates.Done;
                    CharacterDeselect();
                    break;
            }
            _action = null;
        }


        private void CharacterDeselect() {
            _chosenCharacter = null;
            _chosenTile = null;
            _battleHUD.CharacterDeselect();
            _currentInputState = InputState.Idle;
            _totalPathSet.Clear();
            _action = null;

        }

        /// <summary>
        ///     Draws all dat shit man
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Draw(GameTime gameTime) {
            _gameRef.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, DepthStencilState.Default, null, null, Camera2D.TranslationMatrix);

            var heightOffset = 0;

            _gameRef.SpriteBatch.DrawString(_pericles6, _lastTile.Position + " " + _lastTile.Height + " " + _currentInputState, Camera2D.ScreenToWorld(new Vector2(GameRef.Graphics.PreferredBackBufferWidth - 200, 0)), Color.White);


            for (var y = 0; y < _map.Height; ++y) {
                for (var x = 0; x < _map.Width; ++x) {
                    foreach (TileMap layer in _mapLayers) {
                        heightOffset += 10;
                        if (!layer.TileExists(x, y))
                            continue;
                        var tilePosition = new Vector2((x*32 - y*32), (x*16 + y*16) - layer.Tile(x, y).Height);
                        _gameRef.SpriteBatch.Draw(_tileSetTexture, tilePosition, GetSourceRectangle(layer.Tile(x, y).TileId), Color.White);


                        if (_chosenCharacter == null) continue;
                        if (_chosenCharacter.CurrentState != Character.CharacterStates.Moving) //VERY, VERY SPECIFIC FUCKING CASES WHEN THERE'S A MOVING CHARACTER, BECAUSE FUCK Z-BUFFER
                            continue;
                        RedrawForMover(x, y, gameTime, heightOffset, layer);
                    }

                    if (_currentInputState == InputState.CharacterSelected) {
                        foreach (MapCell mapCell in _totalPathSet) {
                            if (mapCell != _myMap.Tile(x, y))
                                continue;
                            int posX = mapCell.Position.X;
                            int posY = mapCell.Position.Y;
                            _gameRef.SpriteBatch.Draw(_hilight, new Vector2((posX*32 - posY*32), (posX*16 + posY*16) - mapCell.Height), Color.Blue*.5f);
                        }
                    }

                    if (_lastTile == _myMap.Tile(x, y))
                        _gameRef.SpriteBatch.Draw(_hilight, _hilightPos, Color.CornflowerBlue*.5f);


                    if (_myMap.Tile(x, y).Occupied)
                        if (_myMap.Tile(x, y).Ocupee.CurrentState != Character.CharacterStates.Moving)
                            _myMap.Tile(x, y).Ocupee.Draw(gameTime, GameRef.SpriteBatch);

                    heightOffset = 0;
                }
            }


            _gameRef.SpriteBatch.End();
            _gameRef.SpriteBatch.Begin();
            _battleHUD.Draw(_gameRef.SpriteBatch);
            DrawHUD();
            if (_currentInputState == InputState.Menu)
                //DrawMenu();
                base.Draw(gameTime);
            _gameRef.SpriteBatch.End();
        }


        private void RedrawForMover(int x, int y, GameTime gameTime, int heightOffset, TileMap layer) {
            MapCell currQueue = _chosenCharacter.SpriteAnimation.CurrentQueue;
            Point currQueuePos = currQueue.Position;
            MapCell lastQueue = _chosenCharacter.SpriteAnimation.LastTile;
            Point lastQueuePos = lastQueue.Position;
            Vector2 tilePosition;
            if (currQueue == _myMap.Tile(x, y) && currQueue.Height == heightOffset) {
                _chosenCharacter.Draw(gameTime, GameRef.SpriteBatch);
            }
            if (currQueuePos.X < lastQueuePos.X) {
                // IF HES MOVING TO THE WEST 
                if (_chosenCharacter.SpriteAnimation.LastTile != _myMap.Tile(x, y) /* || _chosenCharacter.SpriteAnimation.LastTile.Height > currQueue.Height*/)
                    return;
                _chosenCharacter.Draw(gameTime, GameRef.SpriteBatch);
                if (_myMap.Tile(currQueuePos.X + 1, currQueuePos.Y).Height <= _myMap.Tile(currQueuePos.X, currQueuePos.Y).Height)
                    return;
                if (heightOffset == currQueue.Height) {
                    tilePosition = new Vector2(((currQueuePos.X + 1)*32 - currQueuePos.Y*32),
                        ((currQueuePos.X + 1)*16 + currQueuePos.Y*16) - _myMap.Tile(currQueuePos.X + 1, currQueuePos.Y).Height);
                    _gameRef.SpriteBatch.Draw(_tileSetTexture, tilePosition,
                        GetSourceRectangle(_myMap.Tile(currQueuePos.X + 1, currQueuePos.Y).TileId), Color.White);
                    if (_myMap.Tile(currQueuePos.X + 1, currQueuePos.Y).Occupied)
                        _myMap.Tile(currQueuePos.X + 1, currQueuePos.Y).Ocupee.Draw(gameTime, GameRef.SpriteBatch);
                }
            }
            else if (currQueuePos.Y < lastQueuePos.Y) {
                // IF HES MOVING TO THE NORTH
                if (lastQueue == _myMap.Tile(x, y) && _chosenCharacter.SpriteAnimation.LastTile.Height <= currQueue.Height)
                    _chosenCharacter.Draw(gameTime, GameRef.SpriteBatch);
                if (_chosenCharacter.SpriteAnimation.LastTile != _myMap.Tile(x, y))
                    return; //if its not the same tile I was on, continue
                if (_myMap.Tile(currQueuePos.X + 1, currQueuePos.Y).Height > currQueue.Height) {
                    //if the one to his right is higher 

                    if (_myMap.Tile(lastQueuePos.X + 1, lastQueuePos.Y).Height < _myMap.Tile(currQueuePos.X + 1, currQueuePos.Y).Height) {
                        // the one before is lower height
                        foreach (TileMap mapLayer in _mapLayers.Where
                            (mapLayer => mapLayer.TileExists(currQueuePos.X + 1, currQueuePos.Y)).Where
                            (mapLayer => _myMap.Tile(lastQueuePos.X + 1, lastQueuePos.Y).Height < mapLayer.Tile(currQueuePos.X + 1, currQueuePos.Y).Height)) {
                            tilePosition = new Vector2(((currQueuePos.X + 1)*32 - (currQueuePos.Y)*32), ((currQueuePos.X + 1)*16 + (currQueuePos.Y)*16) -
                                                                                                        mapLayer.Tile(currQueuePos.X + 1, currQueuePos.Y).Height);
                            _gameRef.SpriteBatch.Draw(_tileSetTexture, tilePosition, GetSourceRectangle(mapLayer.Tile(currQueuePos.X + 1, currQueuePos.Y).TileId, 32), Color.White);
                        }
                        //} else if(_myMap.Tile(lastQueuePos.X + 1, lastQueuePos.Y).Height > _myMap.Tile(currQueuePos.X + 1, currQueuePos.Y).Height) { //the one before is higher height, don't need it.
                    }
                    else if (_myMap.Tile(lastQueuePos.X + 1, lastQueuePos.Y).Height == _myMap.Tile(currQueuePos.X + 1, currQueuePos.Y).Height) {
                        //DONE, SAME HEIGHT
                        tilePosition = new Vector2(((currQueuePos.X + 1)*32 - (currQueuePos.Y)*32), ((currQueuePos.X + 1)*16 + (currQueuePos.Y)*16) -
                                                                                                    _myMap.Tile(currQueuePos.X + 1, currQueuePos.Y).Height);
                        _gameRef.SpriteBatch.Draw(_tileSetTexture, tilePosition, GetSourceRectangle(layer.Tile(currQueuePos.X + 1, currQueuePos.Y).TileId, 32), Color.White);
                    }
                }

                if (!_myMap.TileExists(currQueuePos.X + 1, currQueuePos.Y))
                    return;
                if (_myMap.Tile(currQueuePos.X + 1, currQueuePos.Y).Occupied) //CHECKS IF THAT SPOT I JUST REDREW IS OCCUPIED AND REDRAWS IT
                    _myMap.Tile(currQueuePos.X + 1, currQueuePos.Y).Ocupee.Draw(gameTime, GameRef.SpriteBatch);
            }
            else if (currQueuePos.Y > lastQueuePos.Y) {
                // IF HES MOVING TO THE SOUTH
                if (currQueue != _myMap.Tile(x, y))
                    return;
                if (currQueue.Height != heightOffset)
                    return;
                if (_myMap.Tile(currQueuePos.X + 1, currQueuePos.Y - 1).Height <= currQueue.Height)
                    return; //CHECKS IF THE X+1, Y-1 TILE'S HEIGHT IS HIGHER THAN THE HEIGHT THE SPRITE IS ON

                if (_myMap.Tile(currQueuePos.X + 1, currQueuePos.Y).Height == _myMap.Tile(currQueuePos.X + 1, currQueuePos.Y - 1).Height) {
                    tilePosition = new Vector2(((currQueuePos.X + 1)*32 - (currQueuePos.Y - 1)*32),
                        ((currQueuePos.X + 1)*16 + (currQueuePos.Y - 1)*16) - _myMap.Tile(currQueuePos.X + 1, currQueuePos.Y - 1).Height);
                    _gameRef.SpriteBatch.Draw(_tileSetTexture, tilePosition,
                        GetSourceRectangle(layer.Tile(currQueuePos.X + 1, currQueuePos.Y - 1).TileId, 32, 32), Color.White);
                }
                else if (_myMap.Tile(currQueuePos.X + 1, currQueuePos.Y).Height < _myMap.Tile(currQueuePos.X + 1, currQueuePos.Y - 1).Height) {
                    //leave this shit alone i barely even understand it myself lmfao
                    foreach (TileMap mapLayer in _mapLayers.Where(mapLayer => mapLayer.TileExists(currQueuePos.X + 1, currQueuePos.Y - 1)).Where
                        (mapLayer => mapLayer.Tile(currQueuePos.X + 1, currQueuePos.Y - 1).Height > _myMap.Tile(currQueuePos.X + 1, currQueuePos.Y).Height)) {
                        tilePosition = new Vector2(((currQueuePos.X + 1)*32 - (currQueuePos.Y - 1)*32),
                            ((currQueuePos.X + 1)*16 + (currQueuePos.Y - 1)*16) - mapLayer.Tile(currQueuePos.X + 1, currQueuePos.Y - 1).Height);
                        _gameRef.SpriteBatch.Draw(_tileSetTexture, tilePosition, GetSourceRectangle(layer.Tile(currQueuePos.X + 1, currQueuePos.Y - 1).TileId, 32),
                            Color.White);
                    }
                }
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="tileIndex"></param>
        /// <param name="tileWidth"></param>
        /// <param name="mTileSize"></param>
        /// <returns></returns>
        public Rectangle GetSourceRectangle(int tileIndex, int tileWidth = 64, int mTileSize = 42) {
            int tileY = tileIndex/(_tileSetTexture.Width/_map.TileWidth);
            int tileX = tileIndex%(_tileSetTexture.Width/_map.TileWidth);
            return new Rectangle(tileX*_map.TileWidth, tileY*42, tileWidth, mTileSize);
        }

        #endregion

        #region Virtual Method region

        #endregion
    }
}