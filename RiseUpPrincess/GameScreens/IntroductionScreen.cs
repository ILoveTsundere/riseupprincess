﻿using Microsoft.Xna.Framework;
using XRPGLibrary;

namespace RiseUpPrincess.GameScreens {
    /// <summary>
    ///     THIS IS THE WORLD VIEW SCREEN, MAP WILL BE LOADED AND PLAYER WILL CHOOSE THE NEXT PART OF THE ADVENTURE
    ///     OR JUST MAKE CHANGES TO ARMY ETC
    /// </summary>
    public class IntroductionScreen : BaseGameState {
        #region Constructor Region

        public IntroductionScreen(Game1 game, GameStateManager manager)
            : base(game, manager) {
        }

        #endregion

        #region Field Region

        #endregion

        #region Property Region

        #endregion

        #region Method Region

        protected override void LoadContent() {
            base.LoadContent();
        }

        public override void Update(GameTime gameTime) {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime) {
            base.Draw(gameTime);
        }

        #endregion
    }
}