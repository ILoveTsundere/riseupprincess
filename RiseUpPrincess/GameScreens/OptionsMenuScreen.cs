﻿using XRPGLibrary;

namespace RiseUpPrincess.GameScreens {
    public class OptionsMenuScreen : BaseGameState {
        #region Constructor Region

        public OptionsMenuScreen(Game1 game, GameStateManager manager)
            : base(game, manager) {
        }

        #endregion

        #region Field Region

        #endregion

        #region Property Region

        #endregion

        #region Method Region

        #endregion

        #region Virtual Method region

        #endregion
    }
}