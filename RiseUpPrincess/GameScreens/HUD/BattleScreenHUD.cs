﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using XRPGLibrary.CharacterClasses;
using XRPGLibrary.Controls;

namespace RiseUpPrincess.GameScreens.HUD {
    public sealed class BattleScreenHUD {
        #region Constructor Region

        public BattleScreenHUD(ControlManager controlManager, Game1 gameRef, BattleScreen battle) {
            _chosenChar = null;
            _battleScreen = battle;
            _isSecondaryMenuListed = false;
            _turnLabel = new Label {Text = "Turn 0"};
            _turnLabel.Position = new Vector2((gameRef.Graphics.PreferredBackBufferWidth - _turnLabel.SpriteFont.MeasureString(_turnLabel.Text).X)/2, 10);
            _controlManager = controlManager;
            _actionBox = new ListBox(gameRef.Content.Load<Texture2D>(BattleScreenLeftHUD));
            _actionBox.Enter += OnActionBoxEnter;
            _actionBox.SelectionChanged += OnActionBoxSelectionChanged;
            _actionBox.Leave += OnActionBoxLeave;
            _actionBox.Selected += OnActionBoxSelected;
            //_battleScreenMenu = new MenuBox(null); //TODO: MENU
            _controlManager.Add(_actionBox);
            _controlManager.Add(_turnLabel);
            _actionBox.Visible = false;
            _endTurnButton = new Button(gameRef.Content.Load<Texture2D>("GUI/Buttons/EndTurnButton"));
            _actionBox.SetEffect(new FadeEffect());
        }

        #endregion



        #region Field Region

        private const string BattleScreenLeftHUD = "GUI/HUD/test";
        private readonly ListBox _actionBox; //change to picturebox later
        private Character _chosenChar;
        private PictureBox _charPortrait; //portrait 
        private readonly Label _turnLabel; //label that contains the current turn #, will always be shown
        private Label _transitioningLabel; //label will show up in the middle of the screen whenever the turns control shifts
        private readonly ControlManager _controlManager;
        private readonly BattleScreen _battleScreen;
        private MenuBox _battleScreenMenu;
        private string _lastSelectedMenu;
        private Button _endTurnButton;
        private ControlPanel _actionInfoPanel;
        private bool _isSecondaryMenuListed;
        private bool _isPaused;
        #endregion

        #region Property Region

        /// <summary>
        ///     Bool for checking if the menu for skills, items and magic is being shown.
        ///     Using
        /// </summary>
        #endregion

        #region Method Region

        public void Update(GameTime gameTime) {
            _controlManager.Update(gameTime, PlayerIndex.One);
            /*if (_chosenChar == null) return;
            switch (_chosenChar.CurrentState) {
                case Character.CharacterStates.Moved: //it means he moved, he can still attack 
                    
                    break;

                case Character.CharacterStates.Idle: //all his options are available
                    break;

                default: //he can't do shit anymore, this probably won't happen.

                    break;
            }*/
        }


        /// <summary>
        ///     Toggles the menu visibility on or off, also does other things... like dim the screen and shit.
        /// </summary>
        public void ToggleMenu() {
            _isPaused = !_isPaused;
            
        }

        public void ChosenCharacter(Character charac) {
            if (charac == null) return;
            if (_charPortrait == null) {
                _charPortrait = new PictureBox(charac.PortraitImage);
                _charPortrait.SetPosition(new Vector2(20, 700));
                _controlManager.Add(_charPortrait);
            }
            else {
                _charPortrait.ImageSwap(charac.PortraitImage);
                _charPortrait.Visible = true;
            }
            _chosenChar = charac;
            _actionBox.HasFocus = true;
            _actionBox.SelectedIndex = 0;

        }


        private void OnActionBoxSelected(object sender, EventArgs e) {
            switch (_actionBox.SelectedItem) {
                case "Move":
                    _battleScreen.CheckPathForAction("Move");

                    break;
                case "Skills":
                    ShowCharacterSkills();
                    break;
                case "Attack":
                    _battleScreen.CheckPathForAction("Attack");
                    break;
                case "Defend":
                    _battleScreen.CheckPathForAction("Defend");
                    break;
                case "Magic":
                    ShowCharacterMagic();
                    break;
                case "Stay":
                    _battleScreen.CheckPathForAction("Stay");
                    break;
                default: //a skill or shit has been chosen
                    switch (_lastSelectedMenu) {
                        case "Items":
                            //go on chosenchar's list of items and find the item
                            break;
                        case "Magic":
                            //go on the list and find the spell
                            break;
                        case "Skills":
                            //_chosenChar.SkillList.IndexOf(temp.SelectedItem);
                            //go on the list of skills and find the skill
                            break;
                    }
                    break;
            }
        }

        public void CharacterDeselect () {
            if(_actionBox.HasFocus)
                _actionBox.HasFocus = false;
        }

        /// <summary>
        ///     Toggles the menu to end the turn and maybe other things...?
        /// </summary>
        public void ToggleTurnMenu () {

        }

        /// <summary>
        ///     Will show action's chance of hitting and the effects of it.
        /// </summary>
        public void ShowActionInfo ( Character target, string action ) {
            switch(action) {
                case "Attack":
                    break;

                case "Item":
                    break;

                case "Magic":
                    break;

                case "Skill":
                    break;
            }
        }

        /// <summary>
        ///     Method for when the box gets focus.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnActionBoxEnter(object sender, EventArgs e) {
            var temp = sender as ListBox;
            _actionBox.Visible = true;
            _actionBox.Items.Clear();
            if (_chosenChar.CurrentState == Character.CharacterStates.Idle) {
                _actionBox.Items.Add("Move");
                _actionBox.Items.Add("Attack");
                _actionBox.Items.Add("Skills/Magic");
                _actionBox.Items.Add("Defend");
                _actionBox.Items.Add("Stay");
            } else {
                _actionBox.Items.Add("Attack");
                _actionBox.Items.Add("Skills/Magic");
                _actionBox.Items.Add("Defend");
                _actionBox.Items.Add("Stay");
            }
        }

        private void ShowCharacterSkills() {
            _chosenChar.ShowSkillList();
        }

        private void ShowCharacterMagic() {
            _chosenChar.ShowMagicList();
        }

        /// <summary>
        ///     Method for when the selection is changed, should as special effects or whatever.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnActionBoxSelectionChanged(object sender, EventArgs e) {
            Console.WriteLine(_actionBox.SelectedItem);
        }

        private void OnActionBoxLeave(object sender, EventArgs e) {
            _charPortrait.Visible = false;
            _actionBox.Visible = false;
        }


        public void Draw(SpriteBatch spriteBatch) {
            _controlManager.Draw(spriteBatch);
            //TODO: Draw the HUD and then some parts depending on the character and game's state.
            //if (_chosenChar == null) return;
        }

        #endregion

        #region Virtual Method region

        #endregion
    }
}