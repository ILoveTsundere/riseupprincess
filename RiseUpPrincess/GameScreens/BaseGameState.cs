﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using XRPGLibrary;
using XRPGLibrary.Controls;

namespace RiseUpPrincess.GameScreens {
    public class BaseGameState : GameState {
        #region Constructor Region

        public BaseGameState(Game1 game, GameStateManager manager)
            : base(game, manager) {
            GameRef = game;
            PlayerIndexInControl = PlayerIndex.One;
        }

        #endregion

        #region Field Region

        protected Game1 GameRef;
        protected ControlManager ControlManager;
        protected PlayerIndex PlayerIndexInControl;

        #endregion

        #region Property Region

        #endregion

        #region Method Region

        protected override void LoadContent() {
            ContentManager content = Game.Content;
            var menuFont = content.Load<SpriteFont>(@"Fonts\ControlFont");
            ControlManager = new ControlManager(menuFont);

            base.LoadContent();
        }

        protected override void UnloadContent() {
            base.UnloadContent();
        }

        public override void Update(GameTime gameTime) {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime) {
            base.Draw(gameTime);
        }

        #endregion
    }
}