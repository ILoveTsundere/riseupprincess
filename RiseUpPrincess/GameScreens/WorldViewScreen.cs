﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using XRPGLibrary;
using XRPGLibrary.Controls;

namespace RiseUpPrincess.GameScreens {
    /// <summary>
    ///     THIS IS THE WORLD VIEW SCREEN, MAP WILL BE LOADED AND PLAYER WILL CHOOSE THE NEXT PART OF THE ADVENTURE
    ///     OR JUST MAKE CHANGES TO ARMY ETC
    /// </summary>
    public class WorldViewScreen : BaseGameState {
        #region Constructor Region

        public WorldViewScreen(Game1 game, GameStateManager manager)
            : base(game, manager) {
        }

        #endregion

        #region Field Region

        private PictureBox _worldBackground;
        private PictureBox _armyButton;
        private Texture2D _armyButtonImg;

        #endregion

        #region Property Region

        #endregion

        #region Method Region

        protected override void LoadContent() {
            base.LoadContent();
            _worldBackground = new PictureBox(Game.Content.Load<Texture2D>(@"Backgrounds\worldview"), GameRef.ScreenRectangle);
            _armyButtonImg = Game.Content.Load<Texture2D>(@"GUI\armyButton");
            _armyButton = new PictureBox(_armyButtonImg, GameRef.ScreenRectangle);
            _armyButton.SetPosition(new Vector2(0, GameRef.ScreenRectangle.Height - _armyButtonImg.Height));

            //armyButton.Position = new Vector2(0, 658);
            ControlManager.Add(_worldBackground);
            ControlManager.Add(_armyButton);
        }

        public override void Update(GameTime gameTime) {
            ControlManager.Update(gameTime, PlayerIndexInControl);
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime) {
            GameRef.SpriteBatch.Begin();
            base.Draw(gameTime);
            ControlManager.Draw(GameRef.SpriteBatch);
            GameRef.SpriteBatch.End();
        }

        #endregion
    }
}