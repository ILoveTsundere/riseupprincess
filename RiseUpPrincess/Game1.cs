﻿#region Using Statements

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using RiseUpPrincess.GameComponents;
using RiseUpPrincess.GameScreens;
using XRPGLibrary;
using XRPGLibrary.TileEngine;

#endregion

namespace RiseUpPrincess {
    /// <summary>
    ///     This is the main type for your game
    /// </summary>
    public class Game1 : Game {
        public Game1() {
            Player.GameRef = this;
            Graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            Graphics.PreferredBackBufferWidth = ScreenWidth;
            Graphics.PreferredBackBufferHeight = ScreenHeight;

            Camera2D.ViewportWidth = ScreenWidth;
            Camera2D.ViewportHeight = ScreenHeight;
            //this.IsFixedTimeStep = false;
            //graphics.SynchronizeWithVerticalRetrace = false;
            //cam.ViewportHeight 
            //cam.ViewportWidth = graphics.Viewport.Width;

            this.Window.Title = "RiseUpPrincess!";
            Components.Add(new InputHandler(this));
            stateManager = new GameStateManager(this);
            Components.Add(stateManager);
            ScreenRectangle = new Rectangle(0, 0, ScreenWidth, ScreenHeight);

            //WorldViewScreen = new WorldViewScreen(this, stateManager);
            BattleScreen = new BattleScreen(this, stateManager);

            //GameTitleScreen = new GameTitleScreen(this, stateManager);
            stateManager.ChangeState(BattleScreen);
            //IsMouseVisible = true;
        }

        /// <summary>
        ///     Allows the game to perform any initialization it needs to before starting to run.
        ///     This is where it can query for any required services and load any non-graphic
        ///     related content.  Calling base.Initialize will enumerate through any components
        ///     and initialize them as well.
        /// </summary>
        protected override void Initialize() {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        ///     LoadContent will be called once per game and is the place to load
        ///     all of your content.
        /// </summary>
        protected override void LoadContent() {
            // Create a new SpriteBatch, which can be used to draw textures.
            base.LoadContent();

            SpriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        ///     UnloadContent will be called once per game and is the place to unload
        ///     all content.
        /// </summary>
        protected override void UnloadContent() {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        ///     Allows the game to run logic such as updating the world,
        ///     checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime) {
            //if(GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
            //   Exit();

            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        /// <summary>
        ///     This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime) {
            GraphicsDevice.Clear(ClearOptions.Target | ClearOptions.DepthBuffer, Color.Black, 5.0f, 0);
            base.Draw(gameTime);
        }

        #region Fields

        //public GameTitleScreen GameTitleScreen;
        //public WorldViewScreen WorldViewScreen;
        public BattleScreen BattleScreen;
        //public IntroductionScreen IntroductionScreen;
        private readonly GameStateManager stateManager;
        public GraphicsDeviceManager Graphics;
        public SpriteBatch SpriteBatch;
        public const int ScreenWidth = 1024;
        public const int ScreenHeight = 768;

        public readonly Rectangle ScreenRectangle;

        #endregion
    }
}