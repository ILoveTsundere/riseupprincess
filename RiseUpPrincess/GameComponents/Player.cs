﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using XRPGLibrary.CharacterClasses;
using XRPGLibrary.ItemClasses;

namespace RiseUpPrincess.GameComponents {
    public static class Player {
        #region Field Region

        public static Game1 GameRef;
        //Tile chosenTile;
        //MapLayer map;

        #endregion

        #region Property Region
        public static List<GameItem> PlayerInventory {
            get;
            private set;
        } 
        /// <summary>
        /// </summary>
        public static List<Character> RosterCharacters {
            get;
            set;
        }

        /// <summary>
        /// </summary>
        public static List<Character> BattleRosterCharacters {
            get;
            set;
        }

        #endregion

        #region Constructor Region
        static Player () {
            RosterCharacters = new List<Character>();
            BattleRosterCharacters = new List<Character>();
            PlayerInventory = new List<GameItem>();
        }
        #endregion

        #region Method Region

        public static void Update(GameTime gameTime) {
        }

        public static void Draw(GameTime gameTime) {
            GameRef.SpriteBatch.Begin();
            GameRef.SpriteBatch.End();
        }

        #endregion
    }
}