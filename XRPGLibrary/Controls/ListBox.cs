﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace XRPGLibrary.Controls {
    /// <summary>
    /// ListBox Control TODO: MAKE IT BETTER TO UNDERSTAND/LESS COMPLEX
    /// </summary>
    public sealed class ListBox : Control {
        #region Constructor Region

        public ListBox(Texture2D background, Texture2D cursor) {
            _hasFocus = false;
            TabStop = false;
            this._image = background;
            this._image = cursor;
            this.Size = new Vector2(_image.Width, _image.Height);
            _lineCount = _image.Height/SpriteFont.LineSpacing;
            _startItem = 0;
        }

        public ListBox(Texture2D background) {
            _hasFocus = false;
            TabStop = false;
            this._image = background;
            this.Size = new Vector2(_image.Width, _image.Height);
            _lineCount = _image.Height/SpriteFont.LineSpacing;
            _startItem = 0;
        }

        #endregion

        #region Event Region

        public event EventHandler SelectionChanged;
        public event EventHandler Enter;
        public event EventHandler Leave;

        #endregion

        #region Field Region

        private readonly List<string> _items = new List<string>();
        private int _startItem;
        private readonly int _lineCount;
        private readonly Texture2D _image;
        //Texture2D cursor;
        private Color _selectedColor = Color.Yellow;
        private int _selectedItem;

        #endregion

        #region Property Region

        public Color SelectedColor {
            get { return _selectedColor; }
            set { _selectedColor = value; }
        }

        public int SelectedIndex {
            get { return _selectedItem; }
            set { _selectedItem = (int) MathHelper.Clamp(value, 0f, _items.Count); }
        }

        public string SelectedItem {
            get { return Items[_selectedItem]; }
        }

        public List<string> Items {
            get { return _items; }
        }

        public override bool HasFocus {
            get { return _hasFocus; }
            set {
                _hasFocus = value;
                if (_hasFocus)
                    OnEnter(null);
                else
                    OnLeave(null);
            }
        }

        #endregion

        #region Abstract Method Region

        public override void Update(GameTime gameTime) {
        }

        public override void Draw(SpriteBatch spriteBatch) {
            spriteBatch.Draw(_image, Position, Color.White);
            for (var i = 0; i < _lineCount; i++) {
                if (_startItem + i >= _items.Count)
                    break;
                if (_startItem + i == _selectedItem) {
                    spriteBatch.DrawString(
                        SpriteFont,
                        _items[_startItem + i],
                        new Vector2(Position.X + (_image.Width - SpriteFont.MeasureString(_items[_startItem + i]).X)/2, Position.Y + i*SpriteFont.LineSpacing), SelectedColor*Alpha);
                    //spriteBatch.Draw(cursor, new Vector2(Position.X - (cursor.Width + 2), Position.Y + i * SpriteFont.LineSpacing + 5), Color.White);
                }
                else
                    spriteBatch.DrawString(
                        SpriteFont,
                        _items[_startItem + i],
                        new Vector2(Position.X + (_image.Width - SpriteFont.MeasureString(_items[_startItem + i]).X)/2, 2 + Position.Y + i*SpriteFont.LineSpacing),
                        Color);
            }
        }

        public override void HandleInput(PlayerIndex playerIndex) {
            if (!HasFocus)
                return;
            if(InputHandler.KeyReleased(Keys.S) || InputHandler.KeyReleased(Keys.Down) ||
                InputHandler.ButtonReleased(Buttons.LeftThumbstickDown, playerIndex)) {
                if (_selectedItem < _items.Count - 1) {
                    _selectedItem++;
                    if (_selectedItem >= _startItem + _lineCount)
                        _startItem = _selectedItem - _lineCount + 1;
                    OnSelectionChanged(null);
                }
            } else if(InputHandler.KeyReleased(Keys.W) || InputHandler.KeyReleased(Keys.Up) ||
                     InputHandler.ButtonReleased(Buttons.LeftThumbstickUp, playerIndex)) {
                if (_selectedItem > 0) {
                    _selectedItem--;
                    if (_selectedItem < _startItem)
                        _startItem = _selectedItem;
                    OnSelectionChanged(null);
                }
            }
            if (InputHandler.KeyReleased(Keys.Enter) || InputHandler.KeyReleased(Keys.Space) ||
                InputHandler.ButtonReleased(Buttons.A, playerIndex)) {
                HasFocus = false;
                OnSelected(null);
            }
            if (InputHandler.KeyReleased(Keys.Escape) ||
                InputHandler.ButtonReleased(Buttons.B, playerIndex)) {
                HasFocus = false;
            }
        }

        #endregion

        #region Method Region

        private void OnSelectionChanged(EventArgs e) {
            if (SelectionChanged != null)
                SelectionChanged(this, e);
        }

        private void OnEnter(EventArgs e) {
            if (Enter != null)
                Enter(this, e);
        }

        private void OnLeave(EventArgs e) {
            if (Leave != null)
                Leave(this, e);
        }

        #endregion
    }
}