﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace XRPGLibrary.Controls {
    public class TwitchEffect : ControlEffects{
        #region Field Region
        #endregion
        #region Property Region

        public Vector2 MaxTwitchDistance {
            get;
            set;
        }
        #endregion
        #region Constructor Region

        public TwitchEffect (Vector2 maxTwitchDistance ) {
            MaxTwitchDistance = maxTwitchDistance;
        }
        #endregion
        #region Method Region
        #endregion
        #region Virtual Method region
        #endregion

        #region Overrides of ControlEffects

        public override void Update(GameTime gameTime, Control control) {
            
        }

        #endregion
    }
}
