﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace XRPGLibrary.Controls {
    public class LinkLabel : Control {
        #region Constructor Region

        public LinkLabel() {
            TabStop = true;
            _hasFocus = false;
            Position = Vector2.Zero;
        }

        #endregion

        // drawing a flat colored rectangle
        //    whiteRectangle = new Texture2D(GraphicsDevice, 1, 1);
        //    whiteRectangle.SetData(new[] { Color.White });

        #region Fields and Properties

        private Color _selectedColor = Color.Red;

        public Color SelectedColor {
            get { return _selectedColor; }
            set { _selectedColor = value; }
        }

        #endregion

        #region Abstract Methods

        public override void Update(GameTime gameTime) {
        }

        public override void Draw(SpriteBatch spriteBatch) {
            spriteBatch.DrawString(SpriteFont, Text, Position, HasFocus ? _selectedColor*Alpha : Color);
        }


        public override void HandleInput(PlayerIndex playerIndex) {
            if (!HasFocus)
                return;
            if (InputHandler.KeyReleased(Keys.Enter) ||
                InputHandler.ButtonReleased(Buttons.A, playerIndex))
                OnSelected(null);
        }

        #endregion
    }
}