﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace XRPGLibrary.Controls {
    public class SpinBox : Control {
        #region Constructor Region

        public SpinBox(Texture2D leftArrow, Texture2D rightArrow, Texture2D stop) {
            _minValue = 0;
            _maxValue = 100;
            Increment = 1;
            Width = 50;
            _leftTexture = leftArrow;
            _rightTexture = rightArrow;
            _stopTexture = stop;
            TabStop = true;
        }

        #endregion

        #region Event Region

        public event EventHandler SelectionChanged;

        #endregion

        #region Field Region

        private int _current;
        private int _minValue;
        private int _maxValue;
        private readonly Texture2D _leftTexture;
        private readonly Texture2D _rightTexture;
        private readonly Texture2D _stopTexture;
        private Color _selectedColor = Color.Red;

        #endregion

        #region Property Region

        public int MinimumValue {
            get { return _minValue; }
            set {
                if (value > _maxValue)
                    _minValue = _maxValue;
                else
                    _minValue = value;
            }
        }

        public int MaximumValue {
            get { return _maxValue; }
            set {
                if (value < _minValue)
                    _maxValue = _minValue;
                else
                    _maxValue = value;
            }
        }

        public new int Value {
            get { return _current; }
            set {
                if (value < _minValue)
                    _current = _minValue;
                else if (value > _maxValue)
                    _current = _maxValue;
                else
                    _current = value;
            }
        }

        public int Increment {
            get;
            set;
        }

        public int Width {
            get;
            set;
        }

        public Color SelectedColor {
            get { return _selectedColor; }
            set { _selectedColor = value; }
        }

        #endregion

        #region Method Region

        #endregion

        #region Virtual Method region

        public override void Update(GameTime gameTime) {
        }

        public override void Draw(SpriteBatch spriteBatch) {
            Vector2 drawTo = Position;
            spriteBatch.Draw(_current != _minValue ? _leftTexture : _stopTexture, drawTo, Color.White);
            drawTo.X += _leftTexture.Width + 5f;
            string currentValue = _current.ToString();
            float itemWidth = SpriteFont.MeasureString(currentValue).X;
            float offset = (Width - itemWidth)/2;
            drawTo.X += offset;
            spriteBatch.DrawString(SpriteFont, currentValue, drawTo, HasFocus ? _selectedColor*Alpha : Color);
            drawTo.X += -1*offset + Width + 5f;
            spriteBatch.Draw(_current != _maxValue ? _rightTexture : _stopTexture, drawTo, Color.White);
        }

        public override void HandleInput(PlayerIndex playerIndex) {
            if (InputHandler.ButtonReleased(Buttons.LeftThumbstickLeft, playerIndex) ||
                InputHandler.ButtonReleased(Buttons.DPadLeft, playerIndex) ||
                InputHandler.KeyReleased(Keys.Left)) {
                _current -= Increment;
                if (_current < _minValue)
                    _current = _minValue;
                OnSelectionChanged();
            }
            if (InputHandler.ButtonReleased(Buttons.LeftThumbstickRight, playerIndex) ||
                InputHandler.ButtonReleased(Buttons.DPadRight, playerIndex) ||
                InputHandler.KeyReleased(Keys.Right)) {
                _current += Increment;
                if (_current > _maxValue)
                    _current = _maxValue;
                OnSelectionChanged();
            }
        }

        protected virtual void OnSelectionChanged() {
            if (SelectionChanged != null)
                SelectionChanged(this, null);
        }

        #endregion
    }
}