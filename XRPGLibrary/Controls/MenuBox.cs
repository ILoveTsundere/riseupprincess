﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace XRPGLibrary.Controls {
    public class MenuBox : Control {
        private Texture2D _image;
        private Rectangle _menuBox;
        private int _selectedItem;
        public Color SelectedColor;
        private readonly List<Button> _buttonList;

        public MenuBox(Texture2D image) {
            _image = image;
            _menuBox = new Rectangle(0, 0, image.Width, image.Height);
            _buttonList = new List<Button>();
            TabStop = false;
        }

        public event EventHandler SelectionChanged;
        public event EventHandler Enter;
        public event EventHandler Leave;

        #region Overrides of Control

        /// <summary>
        ///     Adds a new button to the list of buttons and readjusts the positions
        /// </summary>
        /// <param name="text"></param>
        /// <param name="image"></param>
        public void AddButton(string text, Texture2D image) {
            var tempButton = new Button(text, image);
            _buttonList.Add(tempButton);
        }

        public override void Update(GameTime gameTime) {
        }


        public override void Draw(SpriteBatch spriteBatch) {
        }

        public override void HandleInput(PlayerIndex playerIndex) {
            if (!HasFocus)
                return;
            if (InputHandler.KeyReleased(Keys.S) ||
                InputHandler.ButtonReleased(Buttons.LeftThumbstickDown, playerIndex)) {
                if (_selectedItem < _buttonList.Count - 1) {
                    _selectedItem++;
                    OnSelectionChanged(null);
                }
            }
            else if (InputHandler.KeyReleased(Keys.W) ||
                     InputHandler.ButtonReleased(Buttons.LeftThumbstickUp, playerIndex)) {
                if (_selectedItem > 0) {
                    _selectedItem--;
                    OnSelectionChanged(null);
                }
            }
            if (InputHandler.KeyReleased(Keys.Enter) ||
                InputHandler.ButtonReleased(Buttons.A, playerIndex)) {
                HasFocus = false;
                OnSelected(null);
            }
            if (InputHandler.KeyReleased(Keys.Escape) ||
                InputHandler.ButtonReleased(Buttons.B, playerIndex)) {
                HasFocus = false;
            }
        }

        #endregion

        #region Virtual Methods

        protected virtual void OnSelectionChanged(EventArgs e) {
            if (SelectionChanged != null)
                SelectionChanged(this, e);
        }

        protected virtual void OnEnter(EventArgs e) {
            if (Enter != null)
                Enter(this, e);
        }

        protected virtual void OnLeave(EventArgs e) {
            if (Leave != null)
                Leave(this, e);
        }

        #endregion
    }
}