﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace XRPGLibrary.Controls {
    public sealed class Button : Control {
        /// <summary>
        ///     Button with custom text.
        /// </summary>
        /// <param name="text">Button's text</param>
        /// <param name="butImage">Texture2D image</param>
        public Button(string text, Texture2D butImage) {
            _image = butImage;
            _text = text;
        }

        /// <summary>
        ///     Button, no text, usually already on the button.
        /// </summary>
        /// <param name="butImage"></param>
        public Button(Texture2D butImage) {
            _image = butImage;
            _text = null;
        }

        #region Field Region

        private Rectangle _container;
        private readonly Texture2D _image;
        private string _text;

        #endregion

        #region Overrides of Control

        public override void Update(GameTime gameTime) {
            CheckForMouseHover();
        }

        public override void Draw(SpriteBatch spriteBatch) {
            spriteBatch.Draw(_image, Position, _container, Color.White*Alpha);
        }

        public override void HandleInput(PlayerIndex playerIndex) {
            if (!HasFocus) return; //if no focus, fuck off
            if (InputHandler.KeyPressed(Keys.Space))
                OnSelected(null);

            if (!_container.Contains(InputHandler.MouseState.Position)) return; //if it has focus, but the mouse is not within the button, return.
            if (InputHandler.LeftMouseReleased()) {
                OnSelected(null);
            }
        }

        private void CheckForMouseHover() {
            if (!_container.Contains(InputHandler.MouseState.Position)) return;
            OnMouseHover(null);
        }

        #endregion
    }
}