﻿using Microsoft.Xna.Framework;

namespace XRPGLibrary.Controls {
    public abstract class ControlEffects {
        #region Constructor Region

        protected ControlEffects() {
            IsActive = false;
        }

        #endregion

        #region Property Region

        public bool IsActive {
            get;
            set;
        }

        #endregion

        #region Virtual Method region

        public abstract void Update(GameTime gameTime, Control control);

        #endregion

        #region Field Region

        #endregion

        #region Method Region

        #endregion
    }
}