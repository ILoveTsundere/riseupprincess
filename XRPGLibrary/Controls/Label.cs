﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace XRPGLibrary.Controls {
    public class Label : Control {
        #region Constructor Region

        public Label() {
            TabStop = false;
            Color = Color.WhiteSmoke;
        }

        #endregion

        #region Abstract Methods

        public override void Update(GameTime gameTime) {
        }

        public override void Draw(SpriteBatch spriteBatch) {
            spriteBatch.DrawString(SpriteFont, Text, Position, Color*Alpha);
        }

        public override void HandleInput(PlayerIndex playerIndex) {
        }

        #endregion
    }
}