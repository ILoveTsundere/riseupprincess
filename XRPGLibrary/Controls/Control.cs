﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace XRPGLibrary.Controls {
    public abstract class Control {
        #region Constructor Region

        protected Control() {
            Color = Color.Black;
            Enabled = true;
            Visible = true;
            SpriteFont = ControlManager.SpriteFont;
            EffectList = new List<ControlEffects>();
            Alpha = 1f;
        }

        #endregion

        #region Virtual methods

        public virtual void SetEffect(ControlEffects effect) {
            EffectList.Add(effect);
            HasEffect = true;
        }

        #endregion

        #region Event Region

        public event EventHandler Selected;
        public event EventHandler MouseHover;

        #endregion

        #region Virtual Methods

        protected virtual void OnMouseHover(EventArgs e) {
            if (MouseHover != null) {
                MouseHover(this, e);
            }
        }


        protected virtual void OnSelected(EventArgs e) {
            if (Selected != null) {
                Selected(this, e);
            }
        }

        #endregion

        #region Field Region

        public List<ControlEffects> EffectList;
        private Vector2 _position;
        protected bool _hasFocus;

        #endregion

        #region Property Region

        public bool HasEffect {
            get;
            set;
        }

        /// <summary>
        ///     Alpha value of the control.
        /// </summary>
        public float Alpha {
            get;
            set;
        }

        public string Name {
            get;
            set;
        }

        public string Text {
            get;
            set;
        }

        public Vector2 Size {
            get;
            set;
        }

        public Vector2 Position {
            get { return _position; }
            set {
                _position = value;
                _position.Y = (int) _position.Y;
            }
        }

        public object Value {
            get;
            set;
        }

        public virtual bool HasFocus {
            get { return _hasFocus; }
            set { _hasFocus = value; }
        }

        public bool Enabled {
            get;
            set;
        }

        public bool Visible {
            get;
            set;
        }

        public bool TabStop {
            get;
            set;
        }

        public SpriteFont SpriteFont {
            get;
            set;
        }

        public Color Color {
            get;
            set;
        }

        public string Type {
            get;
            set;
        }

        #endregion

        #region Abstract Methods

        public abstract void Update(GameTime gameTime);
        public abstract void Draw(SpriteBatch spriteBatch);
        public abstract void HandleInput(PlayerIndex playerIndex);

        #endregion
    }
}