﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace XRPGLibrary.Controls {
    /// <summary>
    /// Panel class to add controls to and position them relative to the panels position and size.
    /// </summary>
    public class ControlPanel : Control {
        #region Field Region
        #endregion
        #region Property Region

        private List<Control> _controlList;
        #endregion
        #region Constructor Region

        /// <summary>
        /// 
        /// </summary>
        /// <param name="texture"></param>
        public ControlPanel (Texture2D texture) {
            _controlList = new List<Control>();
        }

        public ControlPanel () {
            _controlList = new List<Control>();
        }
        #endregion


        public void AddControl (Control control, Vector2 position) {
            _controlList.Add(control);
            _controlList.Last().Position = position;
        }

        public void SetPosition ( int index, Vector2 position ) {
            _controlList[index].Position = position;
        }
        #region Overrides of Control


        public override void Update(GameTime gameTime) {
            foreach (Control control in _controlList) {
                    if(control.Enabled)
                        control.Update(gameTime);
            }
        }

        public override void Draw(SpriteBatch spriteBatch) {
            foreach (Control control in _controlList) {
                if(control.Visible) {
                    control.Draw(spriteBatch);
                }

            }
        }

        public override void HandleInput(PlayerIndex playerIndex) {
            foreach (Control control in _controlList) {
                if(control.Enabled) {
                    control.HandleInput(playerIndex);
                }
            }
        }

        #endregion
    }
}
