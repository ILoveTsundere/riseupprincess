﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace XRPGLibrary.Controls {
    public class LeftRightSelector : Control {
        #region Constructor Region

        public LeftRightSelector(Texture2D leftArrow, Texture2D rightArrow, Texture2D stop) {
            _leftTexture = leftArrow;
            _rightTexture = rightArrow;
            _stopTexture = stop;
            TabStop = true;
            Color = Color.WhiteSmoke;
        }

        #endregion

        #region Event Region

        public event EventHandler SelectionChanged;

        #endregion

        #region Field Region

        private readonly List<string> _items = new List<string>();
        private readonly Texture2D _leftTexture;
        private readonly Texture2D _rightTexture;
        private readonly Texture2D _stopTexture;
        private Color _selectedColor = Color.Red;
        private int _maxItemWidth;
        private int _selectedItem;

        #endregion

        #region Property Region

        public Color SelectedColor {
            get { return _selectedColor; }
            set { _selectedColor = value; }
        }

        public int SelectedIndex {
            get { return _selectedItem; }
            set { _selectedItem = (int) MathHelper.Clamp(value, 0f, _items.Count); }
        }

        public string SelectedItem {
            get { return Items[_selectedItem]; }
        }

        public List<string> Items {
            get { return _items; }
        }

        #endregion

        #region Method Region

        public void SetItems(string[] items, int maxWidth) {
            this._items.Clear();
            foreach (string s in items)
                this._items.Add(s);
            _maxItemWidth = maxWidth;
        }

        protected void OnSelectionChanged() {
            if (SelectionChanged != null) {
                SelectionChanged(this, null);
            }
        }

        #endregion

        #region Abstract Method Region

        public override void Update(GameTime gameTime) {
        }

        public override void Draw(SpriteBatch spriteBatch) {
            Vector2 drawTo = Position;
            if (_selectedItem != 0)
                spriteBatch.Draw(_leftTexture, drawTo, Color.White);
            else
                spriteBatch.Draw(_stopTexture, drawTo, Color.White);
            drawTo.X += _leftTexture.Width + 5f;
            float itemWidth = SpriteFont.MeasureString(_items[_selectedItem]).X;
            float offset = (_maxItemWidth - itemWidth)/2;
            drawTo.X += offset;
            if (HasFocus)
                spriteBatch.DrawString(SpriteFont, _items[_selectedItem], drawTo, _selectedColor*Alpha);
            else
                spriteBatch.DrawString(SpriteFont, _items[_selectedItem], drawTo, Color);
            drawTo.X += -1*offset + _maxItemWidth + 5f;
            if (_selectedItem != _items.Count - 1)
                spriteBatch.Draw(_rightTexture, drawTo, Color.White);
            else
                spriteBatch.Draw(_stopTexture, drawTo, Color.White);
        }

        public override void HandleInput(PlayerIndex playerIndex) {
            if (_items.Count == 0)
                return;
            if (InputHandler.ButtonReleased(Buttons.LeftThumbstickLeft, playerIndex) ||
                InputHandler.ButtonReleased(Buttons.DPadLeft, playerIndex) ||
                InputHandler.KeyReleased(Keys.Left)) {
                _selectedItem--;
                if (_selectedItem < 0)
                    _selectedItem = 0;
                OnSelectionChanged();
            }
            if (InputHandler.ButtonReleased(Buttons.LeftThumbstickRight, playerIndex) ||
                InputHandler.ButtonReleased(Buttons.DPadRight, playerIndex) ||
                InputHandler.KeyReleased(Keys.Right)) {
                _selectedItem++;
                if (_selectedItem >= _items.Count)
                    _selectedItem = _items.Count - 1;
                OnSelectionChanged();
            }
        }

        #endregion
    }
}