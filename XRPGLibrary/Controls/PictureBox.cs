﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace XRPGLibrary.Controls {
    public class PictureBox : Control {
        #region Field Region

        public event EventHandler MouseHoverEventHandler;

        #endregion

        #region Property Region

        public Texture2D Image {
            get;
            set;
        }

        /// <summary>
        ///     The rectangle the picture will be drawn on.
        /// </summary>
        public Rectangle SourceRectangle {
            get;
            set;
        }

        /// <summary>
        ///     A rectangle that specifies (in texels) the source texels from a texture. Use null to draw the entire texture.
        /// </summary>
        public Rectangle DestinationRectangle {
            get;
            set;
        }

        #endregion

        #region Constructors

        /// <summary>
        ///     Contructor for PictureBox. takes in a texture2d image and a rectangle
        /// </summary>
        /// <param name="image"> Texture2D image to draw </param>
        /// <param name="destination"> Rectangle to draw it on. </param>
        public PictureBox(Texture2D image, Rectangle destination) {
            Image = image;
            DestinationRectangle = destination;
            SourceRectangle = new Rectangle(0, 0, image.Width, image.Height);
            Color = Color.White;
        }

        public PictureBox(Texture2D image) {
            Image = image;
            SourceRectangle = new Rectangle(0, 0, image.Width, image.Height);
            Color = Color.White;
        }

        public PictureBox(Texture2D image, Rectangle destination, Rectangle source) {
            Image = image;
            DestinationRectangle = destination;
            SourceRectangle = source;
            Color = Color.White;
        }

        #endregion

        #region Abstract Method Region

        public override void Update(GameTime gameTime) {
            CheckMouseHover();
        }

        public override void Draw(SpriteBatch spriteBatch) {
            spriteBatch.Draw(Image, DestinationRectangle, Color*Alpha);
        }

        #endregion

        #region Picture Box Methods

        public void SetPosition(Vector2 newPosition) {
            DestinationRectangle = new Rectangle((int) newPosition.X, (int) newPosition.Y, SourceRectangle.Width, SourceRectangle.Height);
        }

        public override void HandleInput(PlayerIndex playerIndex) {
            if (!HasFocus)
                return;
            if (InputHandler.KeyReleased(Keys.Enter) ||
                InputHandler.ButtonReleased(Buttons.A, playerIndex))
                OnSelected(null);
        }

        public void ImageSwap(Texture2D newImage) {
            Image = newImage;
        }

        private void CheckMouseHover() {
            if (SourceRectangle.Contains(InputHandler.MouseState.Position))
                OnMouseHover(null);
        }

        #endregion
    }
}