﻿using Microsoft.Xna.Framework;

namespace XRPGLibrary.Controls {
    public sealed class FadeEffect : ControlEffects {
        #region Constructor Region

        public FadeEffect() {
            FadeSpeed = 2;
            IsActive = true;
        }

        #endregion

        #region Overrides of ControlEffects

        public override void Update(GameTime gameTime, Control control) {
            if (!IsActive) {
                control.Alpha = 1f;
                return;
            }

            if (!_increase)
                control.Alpha = control.Alpha - FadeSpeed*(float) gameTime.ElapsedGameTime.TotalSeconds;
            else
                control.Alpha = control.Alpha + FadeSpeed*(float) gameTime.ElapsedGameTime.TotalSeconds;

            if (control.Alpha >= 1f) {
                _increase = false;
            }
            else if (control.Alpha <= 0f) {
                _increase = true;
            }
        }

        #endregion

        #region Field Region

        #endregion

        #region Property Region

        public float FadeSpeed {
            get;
            set;
        }

        private bool _increase;

        #endregion

        #region Method Region

        #endregion

        #region Virtual Method region

        #endregion
    }
}