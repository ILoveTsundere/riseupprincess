using System;
using System.Collections.Generic;
using System.Linq;

namespace XRPGLibrary.Utilities {
    /// <summary>
    ///     Priority Queue data structure
    /// </summary>
    internal class PriorityQueue<TP, TV> {
        private readonly SortedDictionary<TP, Queue<TV>> _list = new SortedDictionary<TP, Queue<TV>>();

        public bool IsEmpty {
            get { return !_list.Any(); }
        }

        public void Enqueue(TP priority, TV value) {
            Queue<TV> q;
            if (!_list.TryGetValue(priority, out q)) {
                q = new Queue<TV>();
                _list.Add(priority, q);
            }
            q.Enqueue(value);
        }

        public TV Dequeue() {
            // will throw if there isn�ft any first element!
            KeyValuePair<TP, Queue<TV>> pair = _list.First();
            TV v = pair.Value.Dequeue();
            if (pair.Value.Count == 0) // nothing left of the top priority.
                _list.Remove(pair.Key);
            return v;
        }

        public KeyValuePair<TP, Queue<TV>> Peek() {
            if (IsEmpty) throw new InvalidOperationException();
            return _list.First();
        }
    }
}