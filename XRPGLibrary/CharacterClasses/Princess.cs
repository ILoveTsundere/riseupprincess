﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using XRPGLibrary.SpriteClasses;

namespace XRPGLibrary.CharacterClasses {
    public sealed class Princess : Character {

        private const int HealthMultiplier = 15;
        private const int ManaMultiplier = 15;

        #region Constructor Region

        public Princess(string name) : base(name) {
            Endurance = new AttributePair(10);
            Strength = new AttributePair(10);
            Intelligence = new AttributePair(10);
            Constitution = new AttributePair(10);
            Agility = new AttributePair(10);
            HitPoints = new AttributePair(Constitution.CurrentValue * HealthMultiplier);
            ManaPoints = new AttributePair(Willpower.CurrentValue * ManaMultiplier);
            Moves = new AttributePair(5);
            AttackRange = new AttributePair(3);
            this.PortraitImagePath = "Characters/Portraits/Princess_Portrait";
            this.ImagePath = "Characters/Princess";
            Moves.SetMaximum(5);
            this.IsRanged = false;
            AttackMaxHeight = 30;
            CurrentAlliance = Alliance.Player;

        }

        #endregion

        public override SpriteAnimation SpriteAnimation {
            get;
            internal set;
        }

        #region Field Region

        private const int Width = 64;
        private const int Height = 96;

        #endregion

        #region Method Region

        public override void LoadContent() {
            this.Image = GameRef.Content.Load<Texture2D>(this.ImagePath);
            SpriteAnimation = new SpriteAnimation(this.Image);
            SpriteAnimation.AddAnimation("North", 0, 0, Width, Height, 5, 0.15f);
            SpriteAnimation.AddAnimation("West", 0, Height, Width, Height, 5, 0.15f);
            SpriteAnimation.AddAnimation("South", 0, Height*2, Width, Height, 5, 0.15f);
            SpriteAnimation.AddAnimation("East", 0, Height*3, Width, Height, 5, 0.15f);
            SpriteAnimation.CurrentAnimation = "North";

            this.PortraitImage = GameRef.Content.Load<Texture2D>(this.PortraitImagePath);
            //TODO: later on portraits will be animated :)
        }


        public override void Update(GameTime gameTime) {
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch) {
            base.Draw(gameTime, spriteBatch);
        }

        #endregion

        #region Virtual Method region

        #endregion
    }
}