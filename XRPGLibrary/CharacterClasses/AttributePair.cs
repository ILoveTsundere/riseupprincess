﻿namespace XRPGLibrary.CharacterClasses {
    public class AttributePair {
        #region Field Region

        #endregion

        #region Property Region

        public int CurrentValue {
            get;
            private set;
        }

        public int MaximumValue {
            get;
            private set;
        }

        /// <summary>
        /// Value given by modifier.
        /// </summary>
        public int ModifiedValue {
            get;
            set;
        }

        public static AttributePair Zero {
            get { return new AttributePair(); }
        }

        #endregion

        #region Constructor Region

        private AttributePair() {
            this.CurrentValue = 0;
            this.MaximumValue = 0;
        }

        public AttributePair(int maxValue) {
            this.CurrentValue = maxValue;
            this.MaximumValue = maxValue;
        }

        #endregion

        #region Method Region

        public void SetCurrent(int value) {
            this.CurrentValue = value;
            if (this.CurrentValue > this.MaximumValue)
                this.CurrentValue = this.MaximumValue;
        }

        public void SetMaximum(int value) {
            this.MaximumValue = value;
            if (this.CurrentValue > this.MaximumValue)
                this.CurrentValue = this.MaximumValue;
        }

        #endregion
    }
}