﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using XRPGLibrary.CharacterAbilities;
using XRPGLibrary.ItemClasses;
using XRPGLibrary.Modifiers;
using XRPGLibrary.SpriteClasses;
using XRPGLibrary.TileEngine;

namespace XRPGLibrary.CharacterClasses {
    public abstract class Character {
        #region Constructor Region

        protected Character(String name) {
            this.Name = name;
            SkillList = new List<Skill>();
            MagicList = new List<Magic>();
            ItemList = new List<GameItem>();
            AttackMaxHeight = 30;
            CurrentState = CharacterStates.Idle;
            
        }

        #endregion

        #region Field Region

        public enum CharacterStates {
            Idle,
            Moving,
            Moved,
            Attacking,
            Done
        }

        public enum Alliance {
            Player,
            Other,
            Enemy
        }

        public static Game GameRef;
        protected string PortraitImagePath;
        public Texture2D PortraitImage;
        protected string ImagePath;
        protected Texture2D Image;
        protected int AttackMaxHeight;
        protected SpriteAnimation AttackAnimation;
        protected Alliance CurrentAlliance;
        protected List<Modifier> Modifiers;
        #endregion

        #region Property Region

        protected List<Skill> SkillList;
        protected List<Magic> MagicList;
        protected List<GameItem> ItemList; 

        public CharacterStates CurrentState {
            get;
            set;
        }

        //TODO: modified attack range
        public AttributePair AttackRange {
            get;
            protected set;
        }

        public int HeightModifier {
            get;
            set;
        }

        public bool IsRanged {
            get;
            protected set;
        }


        /// <summary>
        ///     Current mapcell the character inhabits.
        /// </summary>
        public MapCell MapLocation {
            get;
            set;
        }

        public string Name {
            get;
            set;
        }

        public AttributePair Moves {
            get;
            protected set;
        }

        //Attributes
        /// <summary>
        ///     Melee damage
        /// </summary>
        public AttributePair Strength {
            get;
            set;
        }

        /// <summary>
        ///     Mana/Stamina
        /// </summary>
        public AttributePair Endurance {
            get;
            set;
        }


        public AttributePair HitPoints {
            get;
            set;
        }

        public AttributePair ManaPoints {
            get;
            set;
        }

        /// <summary>
        ///     Ranged Damage
        /// </summary>
        public AttributePair Agility {
            get;
            set;
        }

        /// <summary>
        ///     Magic Damage
        /// </summary>
        public AttributePair Intelligence {
            get;
            set;
        }

        /// <summary>
        ///     HP
        /// </summary>
        public AttributePair Constitution {
            get;
            set;
        }

        /// <summary>
        /// Willpower, Mana points
        /// </summary>
        public AttributePair Willpower {
            get;
            set;
        }

        public bool IsDead {
            get;
            private set;
        }

        /// <summary>
        ///     The SpriteAnimation for this character.
        /// </summary>
        public abstract SpriteAnimation SpriteAnimation {
            get;
            internal set;
        }

        // Armor properties
        public GameItem Head {
            get;
            protected set;
        }

        public GameItem Body {
            get;
            protected set;
        }

        public GameItem Hands {
            get;
            protected set;
        }

        public GameItem Feet {
            get;
            protected set;
        }

        // Weapon/Shield properties
        public GameItem MainHand {
            get;
            protected set;
        }

        public GameItem OffHand {
            get;
            protected set;
        }

        #endregion

        #region Method Region

        #endregion

        #region Virtual Method region

        public virtual void UseMagic ( string spellName, Character target ) {
            CurrentState = CharacterStates.Attacking;
            // _magicList.Where(spell => spell.Name == spellName) this finds the spell, then do it

            
        }

        public virtual void UseItem ( string itemName, Character target ) {
        
        }

        public virtual void UseSkill ( String skillName, Character target ) {

        }

        public virtual void Attack(Character target) {
            //TODO: attack logic
            SpriteAnimation.CurrentAnimation = "Attack";
            target.GetDamaged(Strength.CurrentValue);
            CurrentState = CharacterStates.Done;
            
        }

        public virtual void Defend (MapCell direction) {
            //Add a 1 turn modifier
        }

        public virtual void Move (Queue<MapCell> queue, MapCell mapCell) {
            SpriteAnimation.MoveQueue(queue, MapLocation);
        }

        public virtual void GetDamaged(int damage) {
            if (HitPoints.CurrentValue < 0)
                IsDead = true;
        }

        public virtual void ShowActionInfo(string action) {
            switch(action) {
            
            }
        }

        public virtual void LoadContent () {
        
        }

        public virtual void Update(GameTime gameTime) {
            SpriteAnimation.Update(gameTime);


            //modifier logic
        }

        public virtual void UpdateModifiers () {
        
        }

        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch) {
            SpriteAnimation.Draw(spriteBatch, this.CurrentState != CharacterStates.Done);
        }

        //WILL PROBABLY NOT BE USED, DEPENDS.
        public virtual bool Equip(GameItem gameItem) {
            //TODO: Do the equip logic
            return false;
        }

        public virtual bool Unequip(GameItem gameItem) {
            //TODO: Do the unequip logic
            return false;
        }

        #endregion

        public List<Skill> ShowSkillList() {
            return SkillList;
        }

        public List<Magic> ShowMagicList() {
            return MagicList;
        }
    }
}