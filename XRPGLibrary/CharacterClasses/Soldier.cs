﻿using XRPGLibrary.CharacterAbilities;
using XRPGLibrary.SpriteClasses;

namespace XRPGLibrary.CharacterClasses {
    public class Soldier : Character {

        private const int HealthMultiplier = 10;
        private const int ManaMultiplier = 10;

        #region Constructor Region

        public Soldier(string name) : base(name) {
            Endurance = new AttributePair(10);
            Strength = new AttributePair(10);
            Intelligence = new AttributePair(10);
            Constitution = new AttributePair(10);
            Agility = new AttributePair(10);
            HitPoints = new AttributePair(Constitution.CurrentValue * HealthMultiplier);
            ManaPoints = new AttributePair(Willpower.CurrentValue * ManaMultiplier);
            Moves = new AttributePair(6);
            AttackRange = new AttributePair(1);
            this.PortraitImagePath = "Characters/Portraits/Princess_Portrait";
            this.ImagePath = "Characters/Princess";
            Moves.SetMaximum(5);
            this.IsRanged = false;
            AttackMaxHeight = 30;
            CurrentAlliance = Alliance.Player;
        }

        #endregion

        #region Property Region

        public override sealed SpriteAnimation SpriteAnimation {
            get;
            internal set;
        }

        #endregion

        #region Field Region

        #endregion

        #region Method Region


        #endregion

        #region Virtual Method region

        #endregion
    }
}