﻿using System.Collections.Generic;
using Microsoft.Xna.Framework.Graphics;

namespace XRPGLibrary.ItemClasses {
    public class GameItemManager {
        #region Field Region

        private readonly Dictionary<string, GameItem> _gameItems = new Dictionary<string, GameItem>();

        #endregion

        #region Constructor Region

        public GameItemManager(SpriteFont spriteFont) {
            SpriteFont = spriteFont;
        }

        #endregion

        #region Property Region

        public Dictionary<string, GameItem> GameItems {
            get { return _gameItems; }
        }

        public static SpriteFont SpriteFont {
            get;
            private set;
        }

        #endregion

        #region Method Region

        #endregion

        #region Virtual Method region

        #endregion
    }
}