﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace XRPGLibrary.ItemClasses {
    public class GameItem {
        #region Constructor Region

        public bool IsBattleUsable {
            get;
            set;
        }

        public bool IsEquippable {
            get;
            set;
        }

        public GameItem(Texture2D image) {
            this._image = image;
        }

        /*public GameItem ( BaseItem item, Texture2D texture, Rectangle? source ) {
            //baseItem = item;
            image = texture;
            sourceRectangle = source;
            type = item.GetType();
        }*/

        #endregion

        #region Method Region

        public void Draw(SpriteBatch spriteBatch) {
            spriteBatch.Draw(_image, Position, SourceRectangle, Color.White);
        }

        #endregion

        #region Field Region

        public Vector2 Position;
        private readonly Texture2D _image;
        //private readonly BaseItem baseItem;

        #endregion

        #region Property Region

        public Texture2D Image {
            get { return _image; }
        }

        public Rectangle? SourceRectangle {
            get;
            set;
        }

        /*public BaseItem Item {
            get {
                return baseItem;
            }
        }*/

        public Type Type {
            get;
            private set;
        }

        #endregion

        #region Virtual Method region

        #endregion
    }
}