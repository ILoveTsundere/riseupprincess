﻿using System;
using Microsoft.Xna.Framework;

namespace XRPGLibrary.SpriteClasses {
    public class FrameAnimation : ICloneable {
        // Amount of time (in seconds) to display each frame
        private float fFrameLength = 0.2f;
        // Amount of time that has passed since we last animated
        private float fFrameTimer;
        // The frame currently being displayed. 
        // This value ranges from 0 to iFrameCount-1
        private int iCurrentFrame;
        // Number of frames in the Animation
        private int iFrameCount = 1;
        // The first frame of the Animation.  We will calculate other
        // frames on the fly based on this frame.
        private Rectangle rectInitialFrame;

        public FrameAnimation(Rectangle FirstFrame, int Frames) {
            NextAnimation = null;
            PlayCount = 0;
            rectInitialFrame = FirstFrame;
            iFrameCount = Frames;
        }

        public FrameAnimation(int X, int Y, int Width, int Height, int Frames) {
            NextAnimation = null;
            PlayCount = 0;
            rectInitialFrame = new Rectangle(X, Y, Width, Height);
            iFrameCount = Frames;
        }

        public FrameAnimation(int X, int Y, int Width, int Height, int Frames, float FrameLength) {
            NextAnimation = null;
            PlayCount = 0;
            rectInitialFrame = new Rectangle(X, Y, Width, Height);
            iFrameCount = Frames;
            fFrameLength = FrameLength;
        }

        public FrameAnimation(int X, int Y,
            int Width, int Height, int Frames,
            float FrameLength, string strNextAnimation) {
            PlayCount = 0;
            rectInitialFrame = new Rectangle(X, Y, Width, Height);
            iFrameCount = Frames;
            fFrameLength = FrameLength;
            NextAnimation = strNextAnimation;
        }

        // The number of times this animation has been played


        // The animation that should be played after this animation

        /// The number of frames the animation contains
        public int FrameCount {
            get { return iFrameCount; }
            set { iFrameCount = value; }
        }

        /// The time (in seconds) to display each frame
        public float FrameLength {
            get { return fFrameLength; }
            set { fFrameLength = value; }
        }

        /// The frame number currently being displayed
        public int CurrentFrame {
            get { return iCurrentFrame; }
            set { iCurrentFrame = MathHelper.Clamp(value, 0, iFrameCount - 1); }
        }

        public int FrameWidth {
            get { return rectInitialFrame.Width; }
        }

        public int FrameHeight {
            get { return rectInitialFrame.Height; }
        }

        /// The rectangle associated with the current
        /// animation frame.
        public Rectangle FrameRectangle {
            get {
                return new Rectangle(
                    rectInitialFrame.X + (rectInitialFrame.Width*iCurrentFrame),
                    rectInitialFrame.Y, rectInitialFrame.Width, rectInitialFrame.Height);
            }
        }

        public int PlayCount {
            get;
            set;
        }

        public string NextAnimation {
            get;
            set;
        }

        object ICloneable.Clone() {
            return new FrameAnimation(this.rectInitialFrame.X, this.rectInitialFrame.Y,
                this.rectInitialFrame.Width, this.rectInitialFrame.Height,
                this.iFrameCount, this.fFrameLength, NextAnimation);
        }

        public void Update(GameTime gameTime) {
            fFrameTimer += (float) gameTime.ElapsedGameTime.TotalSeconds;

            if (fFrameTimer > fFrameLength) {
                fFrameTimer = 0.0f;
                iCurrentFrame = (iCurrentFrame + 1)%iFrameCount;
                if (iCurrentFrame == 0)
                    PlayCount = (int) MathHelper.Min(PlayCount + 1, int.MaxValue);
            }
        }
    }
}