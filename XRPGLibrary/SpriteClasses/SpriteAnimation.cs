﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using XRPGLibrary.TileEngine;

namespace XRPGLibrary.SpriteClasses {
    public class SpriteAnimation {
        private const int MovesPerTile = 10;
        // If set to anything other than Color.White, will colorize
        // the sprite with that color.
        private Color _colorTint = Color.White;
        //TODO: explain this
        private int _elapsedMovementTime;
        //the move queue and the current mapcell that the sprite is moving to
        private Queue<MapCell> _moveQueue;
        // Which FrameAnimation from the dictionary above is playing
        private string _sCurrentAnimation;
        private int _timesMoved;
        // Calcualted center of the sprite
        private Vector2 _v2Center;
        public MapCell CurrentQueue;
        //animate the movement and move the sprite
        public bool IsMoving;
        public MapCell LastTile;

        /// <summary>
        ///     Time between everytime the character moves towards the designaed tile, it's in milliseconds.
        /// </summary>
        public int TimePerMove = 30;

        // Screen Position of the Sprite
        private Vector2 v2Position = new Vector2(0, 0);

        /// <summary>
        ///     Array of vectors, has 1 for each direction. It has the total amount of distance between tiles on each direction
        /// </summary>
        private readonly List<Vector2> _isoDirectionArray = new List<Vector2> {
            new Vector2(32, -16), //+x, -y North
            new Vector2(-32, 16), //-x, +y South
            new Vector2(32, 16), //+x, +y East 
            new Vector2(-32, -16) //-x,-y West
        };

        // Dictionary holding all of the FrameAnimation objects
        // associated with this sprite.
        private readonly Dictionary<string, FrameAnimation> faAnimations = new Dictionary<string, FrameAnimation>();

        public SpriteAnimation(Texture2D texture) {
            DrawOffset = Vector2.Zero;
            DrawDepth = 0.0f;
            this.Texture = texture;
            IsMoving = false;
        }

        /// <summary>
        ///     How many pixels in x,y the sprite will move on each update, dependant on wheter isMoving or not.
        /// </summary>
        public Vector2 Movevement {
            get;
            set;
        }

        /// <summary>
        ///     The current posision of the sprite
        /// </summary>
        public Vector2 Position {
            get { return v2Position; }
            set { v2Position = value; }
        }

        /// The X position of the sprite's upper left corner pixel.
        public int X {
            get { return (int) v2Position.X; }
            set { v2Position.X = value; }
        }

        /// The Y position of the sprite's upper left corner pixel.
        public int Y {
            get { return (int) v2Position.Y; }
            set { v2Position.Y = value; }
        }

        /// Width (in pixels) of the sprite animation frames
        public int Width {
            get;
            private set;
        }

        /// Height (in pixels) of the sprite animation frames
        public int Height {
            get;
            private set;
        }

        /// The texture associated with this sprite.  All FrameAnimations will be
        /// relative to this texture.
        public Texture2D Texture {
            get;
            set;
        }

        /// Color value to tint the sprite with when drawing.  Color.White
        /// (the default) indicates no tinting.
        public Color Tint {
            get { return _colorTint; }
            set { _colorTint = value; }
        }

        /// The FrameAnimation object of the currently playing animation
        public FrameAnimation CurrentFrameAnimation {
            get { return !string.IsNullOrEmpty(_sCurrentAnimation) ? faAnimations[_sCurrentAnimation] : null; }
        }

        /// The string name of the currently playing animaton.  Setting the animation
        /// resets the CurrentFrame and PlayCount properties to zero.
        public string CurrentAnimation {
            get { return _sCurrentAnimation; }
            set {
                if (!faAnimations.ContainsKey(value))
                    return;
                _sCurrentAnimation = value;
                faAnimations[_sCurrentAnimation].CurrentFrame = 0;
                faAnimations[_sCurrentAnimation].PlayCount = 0;
            }
        }

        public Vector2 DrawOffset {
            get;
            set;
        }

        public float DrawDepth {
            get;
            set;
        }

        public void AddAnimation(string name, int x, int y, int width, int height, int frames, float frameLength) {
            faAnimations.Add(name, new FrameAnimation(x, y, width, height, frames, frameLength));
            this.Width = width;
            this.Height = height;
            _v2Center = new Vector2(this.Width/2, this.Height/2);
            //if there's a problem with the frames, this might be it
        }

        public void AddAnimation(string name, int x, int y, int width, int height, int frames,
            float frameLength, string nextAnimation) {
            faAnimations.Add(name, new FrameAnimation(x, y, width, height, frames, frameLength, nextAnimation));
            this.Width = width;
            this.Height = height;
            _v2Center = new Vector2(this.Width/2, this.Height/2);
            //if there's a problem with the frames, this might be it
        }

        public FrameAnimation GetAnimationByName(string name) {
            if (faAnimations.ContainsKey(name)) {
                return faAnimations[name];
            }
            return null;
        }

        /// <summary>
        ///     Makes the sprite face the specified cell's direction.
        /// </summary>
        /// TODO: THiS THING
        public void FaceDirectionOfCell(MapCell currentTile, MapCell cell) {
            if (currentTile.Position - cell.Position == new Point(0, 1))
                _sCurrentAnimation = "North";
            else if (currentTile.Position - cell.Position == new Point(-1, 0))
                _sCurrentAnimation = "East";
            else if (currentTile.Position - cell.Position == new Point(0, -1))
                _sCurrentAnimation = "South";
            else
                _sCurrentAnimation = "West";
        }

        /// <summary>
        ///     Executing a skill animation,  will get called by the specific character using the specific name of the skill
        /// </summary>
        /// <param name="skillname"></param>
        public void SkillAnimation(String skillname) {
        }

        /// <summary>
        ///     Sets the
        /// </summary>
        /// <param name="location"> A queue of the mapcells the character will walk to go to the desired tile.</param>
        /// <param name="lastTile">
        ///     The last tile the character was in, in this case, it's the tile he was at when the method was
        ///     called.
        /// </param>
        public void MoveQueue(Queue<MapCell> location, MapCell lastTile) {
            IsMoving = true;
            _moveQueue = location;
            CurrentQueue = location.Dequeue();
            LastTile = lastTile;
            FaceDirectionOfCell(LastTile, CurrentQueue);
        }

        public void MoveBy(Vector2 move) {
            v2Position += move/MovesPerTile;
            _timesMoved++;
        }

        /// <summary>
        ///     Moves the sprite closer to the specified mapcell.
        /// </summary>
        /// <param name="cell">The mapcell the sprite will be moving towards to.</param>
        private void MoveTowards(MapCell cell) {
            if (_timesMoved >= MovesPerTile) {
                //if the character arrived to the specific cell
                if (_moveQueue.Count == 0) {
                    // if the move queue is empty, then the character finished moving
                    IsMoving = false;
                }
                else {
                    LastTile = cell;
                    CurrentQueue = _moveQueue.Dequeue();
                    FaceDirectionOfCell(LastTile, CurrentQueue);
                }
                _timesMoved = 0; //reset timesMoved to 0
            }
            else {
                //keep moving towards that cell
                if (LastTile.Position - cell.Position == new Point(0, 1))
                    MoveBy(_isoDirectionArray[(int) Direction.North] + new Vector2(0, LastTile.Height - cell.Height));

                else if (LastTile.Position - CurrentQueue.Position == new Point(-1, 0))
                    MoveBy(_isoDirectionArray[(int) Direction.East] + new Vector2(0, LastTile.Height - cell.Height));

                else if (LastTile.Position - CurrentQueue.Position == new Point(0, -1))
                    MoveBy(_isoDirectionArray[(int) Direction.South] + new Vector2(0, LastTile.Height - cell.Height));
                else
                    MoveBy(_isoDirectionArray[(int) Direction.West] + new Vector2(0, LastTile.Height - cell.Height));
            }
        }

        public void Update(GameTime gameTime) {
            if (IsMoving) {
                _elapsedMovementTime += gameTime.ElapsedGameTime.Milliseconds;
                if (_elapsedMovementTime > TimePerMove) {
                    MoveTowards(CurrentQueue);
                    _elapsedMovementTime = 0;
                }
                // If there is not a currently active animation
                if (CurrentFrameAnimation == null) {
                    // Make sure we have an animation associated with this sprite
                    if (faAnimations.Count > 0) {
                        // Set the active animation to the first animation
                        // associated with this sprite
                        var sKeys = new string[faAnimations.Count];
                        faAnimations.Keys.CopyTo(sKeys, 0);
                        CurrentAnimation = sKeys[0];
                    }
                    else {
                        return;
                    }
                }

                // Run the Animation's update method
                CurrentFrameAnimation.Update(gameTime);

                // Check to see if there is a "followup" animation named for this animation
                if (!String.IsNullOrEmpty(CurrentFrameAnimation.NextAnimation)) {
                    // If there is, see if the currently playing animation has
                    // completed a full animation loop
                    if (CurrentFrameAnimation.PlayCount > 0) {
                        // If it has, set up the next animation
                        CurrentAnimation = CurrentFrameAnimation.NextAnimation;
                    }
                }
            }
            else {
                CurrentFrameAnimation.Update(gameTime);
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="spriteBatch"></param>
        /// <param name="action"></param>
        public void Draw(SpriteBatch spriteBatch, bool action) {
            spriteBatch.Draw(Texture,
                Position,
                CurrentFrameAnimation.FrameRectangle, action ? _colorTint : Color.DarkGray, //draws the sprite normally or dark dependant on if its done an action or not.
                Camera2D.Rotation, _v2Center, 1f, SpriteEffects.None, DrawDepth);
        }

        /// <summary>
        ///     Enum will be used by the array of vectors just to make it easier for me, I guess.
        /// </summary>
        private enum Direction {
            North,
            South,
            East,
            West
        }
    }
}