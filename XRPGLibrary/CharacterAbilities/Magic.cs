﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using XRPGLibrary.CharacterClasses;

namespace XRPGLibrary.CharacterAbilities {
    public class Magic : Ability {
        #region Field Region
        #endregion
        #region Property Region
        #endregion
        #region Constructor Region
        public Magic (Texture2D magic, bool isProjectile) : base(isProjectile)  {
        
        }
        #endregion
        #region Method Region
        #endregion
        #region Virtual Method region
        #endregion

        #region Overrides of Abilitiy

        public override void Update(GameTime gameTime) {
            if(IsProjectile) {
            
            }
        }

        public override void Draw(SpriteBatch spriteBatch) {
        }

        public override void Activate(Character target) {
        }

        #endregion
    }
}
