﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using XRPGLibrary.CharacterClasses;

namespace XRPGLibrary.CharacterAbilities {
    public class Skill : Ability {
        #region Field Region
        #endregion
        #region Property Region
        #endregion
        #region Constructor Region
        public Skill (Texture2D texture, bool isProjectile) : base(isProjectile) {
        
        }
        #endregion
        #region Overrides of Ability

        public override void Update(GameTime gameTime) {
            if(IsProjectile) {
            
            }
        }

        public override void Draw(SpriteBatch spriteBatch) {
        }

        public override void Activate(Character target) {
        }

        #endregion
    
    }
}
