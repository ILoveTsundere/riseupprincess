﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using XRPGLibrary.CharacterClasses;

namespace XRPGLibrary.Modifiers {
    public class Modifier {
        #region Field Region

        protected AttributePair Target;
        protected int Damage;
        protected int Turns;
        public bool IsDone;
        private int _changedValue;
        private readonly bool _isDOT;
        public enum Operation {
            Divide,
            Sum,
            Minus,
            Multiply

        }

        private readonly Operation _currentOperator;
        #endregion

        #region Constructor Region

        protected Modifier (int damage,AttributePair target, Operation operation,  int turns = 1, bool isDot = false ) {
            Damage = damage;
            Turns = turns;
            IsDone = false;
            Target = target;
            _isDOT = isDot;
            _currentOperator = operation;
            if(isDot)
                DoEffect();
            else 
                DoBuff();
            
        }

        #endregion
        #region Method Region

        private void DoBuff () {
            switch(_currentOperator) {
                case Operation.Divide:
                    
                    break;

                case Operation.Minus:
                    break;

                case Operation.Multiply:
                    break;

                case Operation.Sum:
                    break;
            }
        }

       #endregion
        #region Virtual Method region
        public virtual void UpdateModifier ( ) {
            if (_isDOT)
                DoEffect();
            if (Turns > 0)
                Turns--;
            else 
                IsDone = true;
            
        }

        private void DoEffect() {
            switch(_currentOperator) {
                case Operation.Divide:
                    break;

                case Operation.Minus:
                    break;

                case Operation.Multiply:
                    break;

                case Operation.Sum:
                    break;
            }
        }


        #endregion
    }
}
