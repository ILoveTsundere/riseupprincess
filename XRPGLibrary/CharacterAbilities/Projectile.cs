﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using XRPGLibrary.TileEngine;

namespace XRPGLibrary.CharacterAbilities {
    /// <summary>
    /// Catch all class for projectiles, can be anything since the caller will be giving it the actual shit.
    /// </summary>
    public class Projectile {
        #region Field Region

        private bool _isHit;
        private MapCell _startingCell;
        private MapCell _endingCell;
        private Texture2D _projectile;
        #endregion

        #region Constructor Region
        public Projectile (ProjectileAnimation projectile, MapCell startingCell, MapCell endingCell) {
            
        }
        #endregion
        #region Method Region

        public void Update ( GameTime gameTime ) {
            
        }
        #endregion
        #region Virtual Method region
        #endregion
    }
}
