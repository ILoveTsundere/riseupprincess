﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using XRPGLibrary.CharacterClasses;

namespace XRPGLibrary.CharacterAbilities {
    /// <summary>
    /// this encompasses magic and skills
    /// </summary>
    public abstract class Ability {
        #region Field Region

        protected bool IsProjectile;
        protected Projectile Projectile;

        protected Ability ( bool isProjectile ) {
            IsProjectile = isProjectile;
        }
        protected Ability () {
            IsProjectile = false;
        }

        #endregion

        #region Property Region

        #endregion

        #region Constructor Region

        #endregion

        #region Method Region

        public abstract void Update(GameTime gameTime);
        public abstract void Draw ( SpriteBatch spriteBatch );
        public abstract void Activate (Character target );

        #endregion

        #region Virtual Method region

        #endregion
    }
}
