using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace XRPGLibrary {
    /// <summary>
    /// </summary>
    public class InputHandler : GameComponent {
        #region Constructor Region

        public InputHandler(Game game)
            : base(game) {
            _keyboardState = Keyboard.GetState();
            GamePadStates = new GamePadState[Enum.GetValues(typeof (PlayerIndex)).Length];
            foreach (PlayerIndex index in Enum.GetValues(typeof (PlayerIndex)))
                GamePadStates[(int) index] = GamePad.GetState(index);
        }

        #endregion

        #region General Method Region

        /// <summary>
        /// </summary>
        public static void Flush() {
            _lastKeyboardState = _keyboardState;
        }

        #endregion

        #region Keyboard Field Region

        private static KeyboardState _keyboardState;
        private static KeyboardState _lastKeyboardState;
        public static MouseState MouseState;
        private static MouseState _lastMouseState;
        public static int LastMouseWheelValue;
        public static int CurrentMouseWheelValue;

        #endregion

        #region Game Pad Field Region

        #endregion

        #region Keyboard Property Region

        public static KeyboardState KeyboardState {
            get { return _keyboardState; }
        }

        public static KeyboardState LastKeyboardState {
            get { return _lastKeyboardState; }
        }

        #endregion

        #region Game Pad Property Region

        public static GamePadState[] GamePadStates {
            get;
            private set;
        }

        public static GamePadState[] LastGamePadStates {
            get;
            private set;
        }

        #endregion

        #region XNA methods

        public override void Initialize() {
            base.Initialize();
        }

        /// <summary>
        /// </summary>
        /// <param name="gameTime"></param>
        public override void Update(GameTime gameTime) {
            _lastKeyboardState = _keyboardState;
            _keyboardState = Keyboard.GetState();
            LastGamePadStates = (GamePadState[]) GamePadStates.Clone();
            foreach (PlayerIndex index in Enum.GetValues(typeof (PlayerIndex)))
                GamePadStates[(int) index] = GamePad.GetState(index);
            _lastMouseState = MouseState;
            MouseState = Mouse.GetState();
            base.Update(gameTime);
        }

        #endregion

        #region Keyboard Region

        /// <summary>
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool KeyReleased(Keys key) {
            return _keyboardState.IsKeyUp(key) && _lastKeyboardState.IsKeyDown(key);
        }

        /// <summary>
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool KeyPressed(Keys key) {
            return _keyboardState.IsKeyDown(key) &&
                   _lastKeyboardState.IsKeyUp(key);
        }

        /// <summary>
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool KeyDown(Keys key) {
            return _keyboardState.IsKeyDown(key);
        }

        #endregion

        #region Game Pad Region

        /// <summary>
        /// </summary>
        /// <param name="button"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static bool ButtonReleased(Buttons button, PlayerIndex index) {
            return GamePadStates[(int) index].IsButtonUp(button) &&
                   LastGamePadStates[(int) index].IsButtonDown(button);
        }

        /// <summary>
        /// </summary>
        /// <param name="button"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static bool ButtonPressed(Buttons button, PlayerIndex index) {
            return GamePadStates[(int) index].IsButtonDown(button) &&
                   LastGamePadStates[(int) index].IsButtonUp(button);
        }

        /// <summary>
        /// </summary>
        /// <param name="button"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static bool ButtonDown(Buttons button, PlayerIndex index) {
            return GamePadStates[(int) index].IsButtonDown(button);
        }

        #endregion

        #region Mouse Events

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public static bool LeftMouseReleased() {
            return (MouseState.LeftButton == ButtonState.Released && _lastMouseState.LeftButton == ButtonState.Pressed);
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public static bool RightMouseReleased() {
            return (MouseState.RightButton == ButtonState.Released && _lastMouseState.RightButton == ButtonState.Pressed);
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public static bool MouseWheelValueChanged() {
            LastMouseWheelValue = CurrentMouseWheelValue;
            CurrentMouseWheelValue = MouseState.ScrollWheelValue;
            return (LastMouseWheelValue > CurrentMouseWheelValue || LastMouseWheelValue < CurrentMouseWheelValue);
        }

        #endregion
    }
}