﻿using System;
using System.Collections.Generic;
using System.Linq;
using XRPGLibrary.Utilities;

namespace XRPGLibrary.TileEngine {
    /// <summary>
    /// </summary>
    public class VectorPath {
        #region Constructor Region

        public VectorPath(TileMap map) {
            _workMap = map;
            _mapCellSet = new HashSet<MapCell>();
            _mapCellQueue = new Queue<MapCell>();
            _nextQueue = new Queue<MapCell>();
        }

        #endregion

        #region Field Region

        private readonly TileMap _workMap;
        private Queue<MapCell> _mapCellQueue;
        private Queue<MapCell> _nextQueue;
        private readonly HashSet<MapCell> _mapCellSet;
        private int _maxMoves;

        #endregion

        #region Method Region

        /// <summary>
        ///     Uses breadth-first search to find the possible tiles the character can move to.
        /// </summary>
        /// <param name="mapCell"> The Mapcell the character is currently on.</param>
        /// <returns>A HashSet of mapcells.</returns>
        public HashSet<MapCell> GetTotalPath(MapCell mapCell) {
            return BreadthFirstSearch(mapCell.Ocupee.Moves.CurrentValue, mapCell);
        }

        /// <summary>
        ///     Breadth first search on the starting tile up
        ///     TODO: Change this into djisktra using height and terrain modifiers and keep track of max move range
        /// </summary>
        /// <param name="maxMoves">max amount of moves</param>
        /// <param name="startingCell"> the starting mapcell for the algorithm </param>
        /// <param name="heightModifier"> heightmodifier for max height difference between tiles</param>
        /// <param name="ignoreTerrain"> Set to ignore terrain penalties or not </param>
        /// <returns></returns>
        private HashSet<MapCell> BreadthFirstSearch(int maxMoves, MapCell startingCell, int heightModifier = 40, bool ignoreTerrain = true) {
            var moves = 0;
            _mapCellQueue.Enqueue(startingCell);
            _maxMoves = maxMoves; //max moves
            while (moves < _maxMoves) {
                while (_mapCellQueue.Count > 0) {
                    MapCell currentNode = _mapCellQueue.Dequeue();
                    foreach (MapCell adjCell in NeighborNodes(currentNode)) {
                        if (!adjCell.Walkable || adjCell.Occupied || Math.Abs(adjCell.Height - currentNode.Height) > heightModifier)
                            continue; //if its not walkable, is occupied or is already in the set, return
                        _mapCellSet.Add(adjCell); //add that mapcell to the set of possible mapcells
                        _nextQueue.Enqueue(adjCell); //add that mapcell to the queue for further exploration
                    }
                }

                moves++;
                Queue<MapCell> tempQueue = _mapCellQueue; //move the reference of _mapCellQueue to a temporary queue
                _mapCellQueue = _nextQueue; //set mapcellqueue to _nextqueue
                _nextQueue = tempQueue; // set _nextqueue as tempqueue (mapcellqueue)
            }

            //clear both queues before returning the generated set of mapcells
            _nextQueue.Clear();
            _mapCellQueue.Clear();
            return _mapCellSet;
        }

        /// <summary>
        ///     Returns a set of mapcells that the unit is able to act upon.
        /// </summary>
        /// <returns></returns>
        public HashSet<MapCell> AttackRange(MapCell mapCell) {
            MapCell startMapcell = mapCell;
            int range = mapCell.Ocupee.AttackRange.CurrentValue;
            if (mapCell.Ocupee.IsRanged) return BreadthFirstSearch(range, startMapcell); //if the character is ranged, he uses diagonal search.
            return ToHashSet(NeighborNodes(startMapcell, range));
        }

        public HashSet<MapCell> GetNeighborTiles(MapCell mapCell) {
            return ToHashSet(NeighborNodes(mapCell, 1));
        }

        public HashSet<MapCell> SkillRange(MapCell mapcell) {
            return null;
        }

        /// <summary>
        ///     Quick converter from IEnumerable to Hashset.
        /// </summary>
        /// <param name="source">The hashset to convert</param>
        /// <returns></returns>
        private static HashSet<T> ToHashSet<T>(IEnumerable<T> source) {
            return new HashSet<T>(source);
        }

        /// <summary>
        ///     Finds the neighbors for the given MapCell.
        /// </summary>
        /// <param name="node"></param>
        /// <param name="range"></param>
        /// <returns></returns>
        /// TODO: Add height modifiers, eventually.
        private IEnumerable<MapCell> NeighborNodes(MapCell node, int range = 1) {
            var temp = new List<MapCell>();
            for (var x = 0; x < range; x++) {
                temp.Add(_workMap.Tile(node.Position.X, node.Position.Y + 1));
                temp.Add(_workMap.Tile(node.Position.X + 1, node.Position.Y));
                temp.Add(_workMap.Tile(node.Position.X - 1, node.Position.Y));
                temp.Add(_workMap.Tile(node.Position.X, node.Position.Y - 1));
            }
            temp.RemoveAll(items => items == null); //removes the ones that are null (outside the map)
            return temp;
        }

        /// <summary>
        ///     Uses A* to find the shortest path to the specified MapCell.
        /// </summary>
        /// <param name="start">Starting MapCell</param>
        /// <param name="end">Ending MapCell</param>
        /// <param name="heightModifier">height modifier, will be using the one for the class later on</param>
        /// <param name="ignoreTerrain">Set to ignore terrain penalties or not</param>
        /// <returns>Queue of MapCells</returns>
        /// TODO: Improve this method.
        public Queue<MapCell> ShortestPathTo(MapCell start, MapCell end, int heightModifier = 40, bool ignoreTerrain = false) {
            var frontier = new PriorityQueue<int, MapCell>();

            frontier.Enqueue(0, start);

            var cameFrom = new List<KeyValuePair<MapCell, MapCell>>();
            var costSoFar = new Dictionary<MapCell, int> {{start, 0}};

            while (!frontier.IsEmpty) {
                MapCell current = frontier.Dequeue();

                //If the current CellMap is the ending CellMap, then reconstruct the path and return the queue of cellmaps
                if (current == end) {
                    var path = new Queue<MapCell>();
                    while (current != start) {
                        path.Enqueue(current);
                        current = cameFrom.First(pair => pair.Value == current).Key;
                    }
                    return path;
                }

                foreach (MapCell neighbor in NeighborNodes(current)) {
                    if (!_mapCellSet.Contains(neighbor)) continue; //if the neighbor is NOT in the mapcellset we created, then we continue, as its not a possible road to take
                    if (Math.Abs(neighbor.Height - current.Height) > heightModifier) continue;
                    int newCost = costSoFar[current] + 1; //TODO: Add shit like tiles that cost 2 moves and crap like that
                    if (!costSoFar.ContainsKey(neighbor) || newCost < costSoFar[neighbor]) {
                        costSoFar.Add(neighbor, newCost);
                        int priority = newCost + Heuristic(neighbor, end);
                        frontier.Enqueue(priority, neighbor);
                        cameFrom.Add(new KeyValuePair<MapCell, MapCell>(current, neighbor));
                    }
                }
            }
            return null; //it should never actully get here, which means that I fucked up something in my logic.
        }

        /// <summary>
        ///     Calculates Heuristic, I'll keep this as its own method since I might eventually make it more complex.
        /// </summary>
        /// <param name="start">Start position of node</param>
        /// <param name="end">End position of node</param>
        /// <returns>Heuristic Value</returns>
        private static int Heuristic(MapCell start, MapCell end) {
            return Math.Abs((start.Position.X - end.Position.X) + Math.Abs(start.Position.Y - start.Position.Y));
        }

        #endregion
    }
}