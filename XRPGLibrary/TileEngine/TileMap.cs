﻿using System.Collections.Generic;

namespace XRPGLibrary.TileEngine {
    /// <summary>
    /// </summary>
    public class MapRow {
        public List<MapCell> Columns = new List<MapCell>();
    }

    /// <summary>
    /// </summary>
    public class TileMap {
        /// <summary>
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public TileMap(int height, int width) {
            _width = width;
            _height = height;
            for (var y = 0; y < height; y++) {
                var thisRow = new MapRow();
                for (var x = 0; x < width; x++) {
                    thisRow.Columns.Add(new MapCell(x, y));
                }
                Rows.Add(thisRow);
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="y"></param>
        /// <param name="x"></param>
        /// <returns></returns>
        public MapCell Tile(int x, int y) {
            return (TileExists(x, y)) ? this.Rows[y].Columns[x] : null;
        }

        /// <summary>
        ///     Checks if the tile exists
        /// </summary>
        /// <param name="y">Rows</param>
        /// <param name="x">Columns</param>
        /// <returns>Returns true or false depending of the tile exists</returns>
        public bool TileExists(int x, int y) {
            //Console.WriteLine(y + " " + x);
            if (x < 0 || y < 0 || y > _height - 1 || x > _width - 1) //checks if out of bounds
                return false;
            return (this.Rows[y].Columns[x].Height != 0); //returns true if the tile's height is not 0 ( no need to check for negative values)
        }

        /// <summary>
        /// </summary>
        /// <param name="tile"></param>
        /// <returns></returns>
        public bool TileExists(MapCell tile) {
            //Console.WriteLine(y + " " + x);
            return (tile.Height != 0);
        }

        /// <summary>
        /// </summary>
        /// <param name="y"></param>
        /// <param name="x"></param>
        /// <param name="id"></param>
        public void AddTile(int x, int y, int id) {
            this.Rows[y].Columns[x].TileId = id;
        }

        #region  Fields

        public List<MapRow> Rows = new List<MapRow>();
        private readonly int _width;
        private readonly int _height;

        #endregion

        #region Properties

        #endregion
    }
}