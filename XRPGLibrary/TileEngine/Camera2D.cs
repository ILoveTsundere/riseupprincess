﻿using System;
using Microsoft.Xna.Framework;

namespace XRPGLibrary.TileEngine {
    /// <summary>
    ///     Camera work class
    /// </summary>
    public static class Camera2D {
        private static Vector2 _centeringOn;

        /// <summary>
        ///     Construct a new Camera class with standard zoom (no scaling)
        /// </summary>
        static Camera2D() {
            Zoom = 1f;
        }

        public static int MapWidth {
            get;
            set;
        }

        public static int MapHeight {
            get;
            set;
        }

        public static int SpriteHeight {
            get;
            set;
        }

        public static int SpriteWidth {
            get;
            set;
        }

        /// <summary>
        ///     Centered Position of the Camera in pixels.
        /// </summary>
        public static Vector2 Position {
            get;
            private set;
        }

        /// <summary>
        ///     Current Zoom level with 1.0f being standard
        /// </summary>
        public static float Zoom {
            get;
            private set;
        }

        /// <summary>
        ///     Current Rotation amount with 0.0f being standard orientation, NOT used, but still kept because I'd like to be able
        ///     to use this later :D
        /// </summary>
        public static float Rotation {
            get;
            private set;
        }

        /// <summary>
        ///     Height and width of the viewport window which we need to adjust
        ///     any time the player resizes the game window.
        /// </summary>
        public static int ViewportWidth {
            get;
            set;
        }

        public static int ViewportHeight {
            get;
            set;
        }

        /// <summary>
        ///     Center of the Viewport which does not account for scale
        /// </summary>
        public static Vector2 ViewportCenter {
            get { return new Vector2(ViewportWidth*0.5f, ViewportHeight*0.5f); }
        }

        /// <summary>
        ///     Create a matrix for the camera to offset everything we draw,
        ///     the map and our objects. since the camera coordinates are where
        ///     the camera is, we offset everything by the negative of that to simulate
        ///     a camera moving. We also cast to integers to avoid filtering artifacts.
        /// </summary>
        public static Matrix TranslationMatrix {
            get {
                return Matrix.CreateTranslation(-(int) Position.X,
                    -(int) Position.Y, 0)*
                       Matrix.CreateRotationZ(Rotation)*
                       Matrix.CreateScale(new Vector3(Zoom, Zoom, 1))*
                       Matrix.CreateTranslation(new Vector3(ViewportCenter, 0));
            }
        }

        /// <summary>
        ///     Call this method with negative values to zoom out
        ///     or positive values to zoom in. It looks at the current zoom
        ///     and adjusts it by the specified amount. If we were at a 1.0f
        ///     zoom level and specified -0.5f amount it would leave us with
        ///     1.0f - 0.5f = 0.5f so everything would be drawn at half size.
        /// </summary>
        /// <param name="amount"> </param>
        public static void AdjustZoom(int amount) {
            if (amount > 0) {
                Zoom += 0.25f;
            }
            else if (amount < 0) {
                Zoom -= 0.25f;
            }

            if (Zoom < 0.50f)
                Zoom = 0.50f;
            else if (Zoom > 2f)
                Zoom = 2f;

            MoveCamera(Position); //this is so the camera adjusts itself
        }

        /// <summary>
        ///     Adjusts the rotation
        /// </summary>
        /// <param name="amount"></param>
        public static void AdjustRotation(float amount) {
        }

        /// <summary>
        ///     Move the camera in an X and Y amount based on the cameraMovement param.
        ///     if clampToMap is true the camera will try not to pan outside of the
        ///     bounds of the map.
        /// </summary>
        /// <param name="cameraMovement"></param>
        /// <param name="clampToMap"></param>
        public static void MoveCamera(Vector2 cameraMovement, bool clampToMap = false) {
            Vector2 newPosition = Position + cameraMovement;

            Position = clampToMap ? MapClampedPosition(newPosition) : newPosition;
        }

        /// <summary>
        /// </summary>
        /// <returns></returns>
        public static Rectangle ViewportWorldBoundary() {
            Vector2 viewPortCorner = ScreenToWorld(new Vector2(0, 0));
            Vector2 viewPortBottomCorner =
                ScreenToWorld(new Vector2(ViewportWidth, ViewportHeight));

            return new Rectangle((int) viewPortCorner.X,
                (int) viewPortCorner.Y,
                (int) (viewPortBottomCorner.X - viewPortCorner.X),
                (int) (viewPortBottomCorner.Y - viewPortCorner.Y));
        }

        /// <summary>
        ///     Returns true or false depending on whether the mouse is within a set distance away from the viewport.
        /// </summary>
        /// <param name="mousePos"></param>
        /// <returns></returns>
        public static bool CheckBoundaryDistance(Vector2 mousePos) {
            return ViewportWidth - mousePos.X < 100 || ViewportWidth - mousePos.X > ViewportWidth - 100 || ViewportHeight - mousePos.Y < 100 || ViewportHeight - mousePos.Y > ViewportHeight - 100;
        }

        public static Vector2 BoundaryDistance(Vector2 mousePos) {
            var test = new Vector2(ViewportWidth - mousePos.X, ViewportHeight - mousePos.Y);
            float testX = test.X;
            float testY = test.Y;
            if (test.X > ViewportWidth - 100) //means the mouse is west
                testX = (test.X - ViewportWidth);
            else if (test.X < ViewportWidth - 100 && test.X > 100)
                testX = 0;
            if (test.Y > ViewportHeight - 100)
                testY = (test.Y - ViewportHeight);
            else if (test.Y < ViewportHeight - 100 && test.Y > 100)
                testY = 0;
            test = new Vector2(testX, testY);
            Console.WriteLine(test + " " + mousePos);

            return test;
        }

        /// <summary>
        ///     Center the camera on specific pixel coordinates
        /// </summary>
        /// <param name="position"></param>
        public static void CenterOn(Vector2 position) {
            Position = position;
        }

        /*
       // Center the camera on a specific cell in the map
       public void CenterOn( MapCell cell )
       {
          Position = CenteredPosition( cell, true );
       }
 
       private Vector2 CenteredPosition( MapCell cell, bool clampToMap = false )
       {
          var cameraPosition = new Vector2( cell.X * Global.SpriteWidth,
             cell.Y * Global.SpriteHeight );
          var cameraCenteredOnTilePosition =
             new Vector2( cameraPosition.X + Global.SpriteWidth / 2,
                 cameraPosition.Y + Global.SpriteHeight / 2 );
          if ( clampToMap )
          {
             return MapClampedPosition( cameraCenteredOnTilePosition );
          }
 
          return cameraCenteredOnTilePosition;
       }
     */

        /// <summary>
        ///     Slowly moves the camera to that vector
        /// </summary>
        public static void UpdateMove(Vector2 pos) {
            Vector2 posi = pos - Position;
            MoveCamera(posi/60);
        }

        // Clamp the camera so it never leaves the visible area of the map.
        //TODO, Change the dependance on MapWidth and MapHeight -> just make another method for it.
        private static Vector2 MapClampedPosition(Vector2 position) {
            var cameraMax = new Vector2(MapWidth*SpriteWidth/2 -
                                        (ViewportWidth/Zoom/2) + 50,
                MapHeight*SpriteHeight -
                (ViewportHeight/Zoom/2));


            return Vector2.Clamp(position,
                new Vector2((ViewportWidth/Zoom - MapWidth*SpriteWidth)/2, ViewportHeight/Zoom/2 - 100),
                cameraMax);
        }

        /// <summary>
        /// </summary>
        /// <param name="worldPosition"></param>
        /// <returns></returns>
        public static Vector2 WorldToScreen(Vector2 worldPosition) {
            return Vector2.Transform(worldPosition, TranslationMatrix);
        }

        /// <summary>
        /// </summary>
        /// <param name="screenPosition"></param>
        /// <returns></returns>
        public static Vector2 ScreenToWorld(Vector2 screenPosition) {
            return Vector2.Transform(screenPosition,
                Matrix.Invert(TranslationMatrix));
        }
    }
}