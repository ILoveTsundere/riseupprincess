﻿using Microsoft.Xna.Framework;
using XRPGLibrary.CharacterClasses;

namespace XRPGLibrary.TileEngine {
    public class MapCell {
        public MapCell(int x, int y) {
            this.Height = 0;
            this.Walkable = true;
            this.Position = new Point(x, y);
        }

        #region Methods

        public void SetOccupant(Character chara) {
            this.Ocupee = chara;
            this.Occupied = true;
        }

        #endregion

        #region Fields

        #endregion

        #region Properties

        public Point Position {
            get;
            protected set;
        }

        public bool Occupied {
            get;
            set;
        }

        public bool Walkable {
            get;
            set;
        }

        public int TileId {
            get;
            set;
        }

        public int Height {
            get;
            set;
        }

        public Character Ocupee {
            get;
            set;
        }

        #endregion
    }
}